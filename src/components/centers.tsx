import Link from "next/link";

function Departments() {
  return (
    <>
      <nav id="sidebar">
        <ul className="list-unstyled menu-categories" id="accordionCenters">
          <li className="menu">
            <a
              href=""
              data-toggle="collapse"
              aria-expanded="true"
              className="dropdown-toggle"
            >
              <div className="">
                <i className="flaticon-bell-28"></i>
                <span>Temples</span>
              </div>
            </a>
          </li>
          <li className="menu">
            <a
              href="#centers"
              data-toggle="collapse"
              aria-expanded="false"
              className="dropdown-toggle"
            >
              <div className="">
                <i className="flaticon-left-dot-menu"></i>
                <span>Ter Kadamba BACE</span>
              </div>
              <div>
                <i className="flaticon-right-arrow"></i>
              </div>
            </a>
            <ul
              className="collapse submenu list-unstyled"
              id="centers"
              data-parent="#accordionCenters"
            >
              <li>
                <Link href={'/home'}>
                  <a> Main </a>
                </Link>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </>
  );
}

export default Departments;
