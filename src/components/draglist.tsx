import { useEffect, useState } from "react";
import styled from "styled-components";
import { DragDropContext } from "react-beautiful-dnd";
import DraggableElement from "./DraggableElement";

const DragDropContextContainer = styled.div`
  padding: 20px;
  border: 4px solid indianred;
  border-radius: 6px;
`;

const ListGrid = styled.div`
  display: flex;
  // grid-template-columns: 0.5fr 0.5fr 0.5fr;
  // grid-gap: 8px;
`;

const removeFromList = (list, index) => {
  const result = Array.from(list);
  const [removed] = result.splice(index, 1);
  return [removed, result];
};

const addToList = (list, index, element) => {
  const result = Array.from(list);
  result.splice(index, 0, element);
  return result;
};

function DragList(props: any) {
  const { pageList, menuArray } = props;
  const lists = menuArray;

  const [elements, setElements] = useState({});
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    props["dragList"]["Pages"] = pageList;
    menuArray.push("Pages");
    setElements(props["dragList"]);
    setLoaded(true);
  }, []);

  const onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }
    const listCopy = { ...elements };

    const sourceList = listCopy[result.source.droppableId];
    const [removedElement, newSourceList] = removeFromList(
      sourceList,
      result.source.index
    );
    listCopy[result.source.droppableId] = newSourceList;
    const destinationList = listCopy[result.destination.droppableId];
    listCopy[result.destination.droppableId] = addToList(
      destinationList,
      result.destination.index,
      removedElement
    );
      console.log("listCopy", listCopy)
    setElements(listCopy);
  };

  if (!loaded) {
    return;
  }

  return (
    <>
      <DragDropContextContainer>
        <DragDropContext onDragEnd={onDragEnd}>
          <ListGrid >
            {lists.map((listKey) => (
              <div  key={listKey} className="col-lg-3">
                   <DraggableElement
                elements={elements[listKey]}
               
                prefix={listKey}
              />
              </div>
           
            ))}
          </ListGrid>
        </DragDropContext>
      </DragDropContextContainer>
    </>
  );
}

export default DragList;
