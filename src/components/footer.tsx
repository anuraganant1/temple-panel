function Footer() {

  return (
    <>
      <footer className="footer-section theme-footer">
        <div className="footer-section-1  sidebar-theme">
        </div>
        <div className="footer-section-2 container-fluid">
          <div className="row">
            <div id="toggle-grid" className="col-xl-7 col-md-6 col-sm-6 col-12 text-sm-left text-center">
              <ul className="list-inline links ml-sm-5">
                <li className="list-inline-item mr-3">
                  <a href="javascript:void(0);">Home</a>
                </li>
              </ul>
            </div>
            <div className="col-xl-5 col-md-6 col-sm-6 col-12">
              <ul className="list-inline mb-0 d-flex justify-content-sm-end justify-content-center mr-sm-3 ml-sm-0 mx-3">
                <li className="list-inline-item  mr-3">
                  <p className="bottom-footer">2022 <a href="https://harisolution.com/">Hari Solution</a></p>
                </li>
                <li className="list-inline-item align-self-center">
                  <div className="scrollTop"><i className="flaticon-up-arrow-fill-1"></i></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </>
  )
}

export default Footer