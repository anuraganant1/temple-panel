import Link from "next/link";
import { useRef } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

function Header() {
  const formikRef = useRef();
  const initialValues = {
    templeName: "",
    website: "",
    email: "",
    templeCode: "",
    templedescription: "",
    address: "",
    eightygNumber: "",
    country: "India",
    status: "0",
  };

  const FormSchema = Yup.object().shape({
    templeName: Yup.string()
      .min(3, "Must be 3 characters or more")
      .max(40, "Must be 40 characters or less")
      .matches(/^[a-zA-Z_ ]*$/, "Characters Only")
      .required("Name Required"),
    website: Yup.string()
      .min(3, "Must be 3 characters or more")
      .max(40, "Must be 40 characters or less")
      .required("Website Required"),
    email: Yup.string()
      .matches(
        /[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/,
        "Invalid Email"
      )
      .required("Valid Email Required"),
    templeCode: Yup.string()
      .min(3, "Must be 3 characters or more")
      .max(40, "Must be 100 characters or less")
      .required("Temple Code Required"),
    templedescription: Yup.string().required("Description Required"),
    address: Yup.string().required("Address Required"),
    eightygNumber: Yup.string().required(" Eightyg Starting Number Required"),
    country: Yup.string().required("Country Required"),
  });

  const handleSubmit = (values, { resetForm }) => {
    console.log("values", values);
    resetForm({ values: "" });
  };

  return (
    <>
      <header className="header navbar fixed-top navbar-expand-sm">
        <Link href={"/"}>
          <a
            className="sidebarCollapse d-none d-lg-block"
            data-placement="bottom"
          >
            ISKCON
          </a>
        </Link>

        <ul className="navbar-nav flex-row">
          <li className="nav-item  d-lg-block d-none ml-lg-4">
            <form className="form-inline" role="search">
              <input
                type="text"
                className="form-control search-form-control"
                placeholder="Search..."
              />
            </form>
          </li>
        </ul>

        <ul className="navbar-nav flex-row ml-lg-3  ml-auto">
          <li className="nav-item dropdown menu-list-dropdown ml-lg-0 mr-lg-2 ml-3 order-lg-0 order-1">
            <Link href={""}>
              <a className="nav-link  user">
                Home
              </a>
            </Link>
          </li>
        </ul>

        <ul className="navbar-nav flex-row ml-lg-3  ml-auto">
          <li className="nav-item dropdown menu-list-dropdown ml-lg-0 mr-lg-2 ml-3 order-lg-0 order-1">
            <a
              className="nav-link dropdown-toggle user"
              id="menuListDropdown"
              data-toggle="dropdown"
              aria-expanded="false"
            >
              Temple
              <i
                className="flaticon-down-arrow"
                style={{ fontSize: 14, paddingLeft: 10 }}
              ></i>
            </a>
            <div
              className="dropdown-menu  position-absolute"
              aria-labelledby="menuListDropdown"
            >
              <Link href={"/home"}>
                <a className="dropdown-item">
                  <i className="mr-1 flaticon-user-6"></i>{" "}
                  <span>Dashbaord</span>
                </a>
              </Link>
              <a
                className="dropdown-item cursor-pt"
                data-toggle="modal"
                data-target="#registerModal"
              >
                <i className="mr-1 flaticon-calendar-bold"></i>{" "}
                <span>Create</span>
              </a>
            </div>
          </li>
        </ul>

        <ul className="navbar-nav flex-row ml-lg-auto">
          <li className="nav-item dropdown user-profile-dropdown ml-lg-0 mr-lg-2 ml-3 order-lg-0 order-1">
            <a
              className="nav-link dropdown-toggle user"
              id="userProfileDropdown"
              data-toggle="dropdown"
              aria-expanded="false"
            >
              <span className="flaticon-user-12"></span>
            </a>
            <div
              className="dropdown-menu  position-absolute"
              aria-labelledby="userProfileDropdown"
            >
              <a className="dropdown-item" href="user_profile.html">
                <i className="mr-1 flaticon-user-6"></i> <span>My Profile</span>
              </a>
              <a className="dropdown-item" href="apps_scheduler.html">
                <i className="mr-1 flaticon-calendar-bold"></i>{" "}
                <span>My Schedule</span>
              </a>
              <a className="dropdown-item" href="apps_mailbox.html">
                <i className="mr-1 flaticon-email-fill-1"></i>{" "}
                <span>My Inbox</span>
              </a>
              <a className="dropdown-item" href="user_lockscreen_1.html">
                <i className="mr-1 flaticon-lock-2"></i>{" "}
                <span>Lock Screen</span>
              </a>
              <div className="dropdown-divider"></div>
              <Link href={"/login"}>
                <a className="dropdown-item">
                  <i className="mr-1 flaticon-power-button"></i>{" "}
                  <span>Log Out</span>
                </a>
              </Link>
            </div>
          </li>
        </ul>
      </header>

      <div
        className="modal fade"
        id="registerModal"
        role="dialog"
        aria-labelledby="registerModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-sm" role="document">
          <div className="modal-content">
            <div className="modal-header" id="registerModalLabel">
              <h4 className="modal-title">Add Temple</h4>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-hidden="true"
              >
                ×
              </button>
            </div>
            <div className="modal-body text-left">
              <Formik
                innerRef={formikRef}
                initialValues={initialValues}
                validationSchema={FormSchema}
                onSubmit={handleSubmit}
              >
                {({ values }) => (
                  <Form className="form-horizontal1 form-white form-1col text-black">
                    <div className="row">
                      <div className=" col-sm-6 mb-30">
                        <label htmlFor="">Name</label>
                        <Field
                          type="text"
                          className="form-control"
                          name="templeName"
                          placeholder="Enter Name *"
                        />
                        <p className="formError">
                          <ErrorMessage name="templeName" />
                        </p>
                      </div>
                      <div className=" col-sm-6 mb-30">
                        <label htmlFor="">Website</label>
                        <Field
                          type="text"
                          className="form-control"
                          name="website"
                          placeholder="Enter Website *"
                        />
                        <p className="formError">
                          <ErrorMessage name="website" />
                        </p>
                      </div>
                      <div className="col-sm-6 mb-30">
                        <label htmlFor="">Email</label>
                        <Field
                          type="email"
                          className="form-control"
                          name="email"
                          placeholder="Enter Email Id *"
                        />
                        <p className="formError">
                          <ErrorMessage name="email" />
                        </p>
                      </div>

                      <div className="col-sm-6 mb-30">
                        <label htmlFor="">Temple Code</label>
                        <Field
                          type="text"
                          className="form-control"
                          name="templeCode"
                          placeholder="Enter Temple Code *"
                        />
                        <p className="formError">
                          <ErrorMessage name="templeCode" />
                        </p>
                      </div>

                      <div className=" col-sm-12 mb-30">
                        <label htmlFor="">Temple Description </label>
                        <Field
                          type="text"
                          component="textarea"
                          className="form-control"
                          name="templedescription"
                          placeholder="Enter Temple Description"
                        />
                        <p className="formError">
                          <ErrorMessage name="templedescription" />
                        </p>
                      </div>

                      <div className=" col-sm-12 mb-30">
                        <label htmlFor="">Address </label>
                        <Field
                          type="text"
                          component="textarea"
                          className="form-control"
                          name="address"
                          placeholder="Enter Temple Address"
                        />
                        <p className="formError">
                          <ErrorMessage name="address" />
                        </p>
                      </div>

                      <div className="col-sm-6 mb-30">
                        <label htmlFor="">Eightyg Number</label>
                        <Field
                          type="text"
                          className="form-control"
                          name="eightygNumber"
                          placeholder="Enter Eightyg Starting Number *"
                        />
                        <p className="formError">
                          <ErrorMessage name="eightygNumber" />
                        </p>
                      </div>

                      <div className="col-sm-6 mb-30">
                        <label htmlFor="">Country</label>
                        <Field
                          type="text"
                          className="form-control"
                          name="country"
                          placeholder="Enter Country *"
                          disabled={true}
                        />
                        <p className="formError">
                          <ErrorMessage name="country" />
                        </p>
                      </div>

                      <div className="col-sm-6 mb-30">
                        <label className="form-check form-check-inline">
                          <Field
                            className="form-check-input"
                            type="radio"
                            name="status"
                            value="1"
                          />
                          <span className="form-check-label">Active </span>
                        </label>
                        <label className="form-check form-check-inline">
                          <Field
                            className="form-check-input"
                            type="radio"
                            name="status"
                            value="0"
                          />
                          <span className="form-check-label">Deactive </span>
                        </label>
                      </div>

                      <div className="col-sm-12 text-center">
                        <button
                          type="submit"
                          className="btn btn-success btn-rounded"
                        >
                          Submit
                        </button>
                      </div>

                      <div className="col-sm-12 contiontext m-b"></div>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Header;
