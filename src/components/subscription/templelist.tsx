import Image from "next/image"
export default function TempleList (){
return(
  <>
   <div className="row margin-bottom-120">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="statbox widget box box-shadow">
                            <div className="widget-header">
                                <div className="row">
                                    <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>Temple List </h4>
                                    </div>
                                </div>
                            </div>
                            <div className="widget-content widget-content-area">
                                <div className="table-responsive mb-4">
                                    <table id="ecommerce-product-list" className="table  table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Category</th>
                                                <th>Sku</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th className="align-center">Status</th>
                                                <th className="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>0010</td>
                                                <td className="text-center">
                                                  <a className="product-list-img" href="javascript: void(0);">
                                                    <Image src="/assets/img/60x60.jpg" alt="product" height={60} width={60} />
                                                  </a>
                                                </td>
                                                <td>CK Glasses</td>
                                                <td>Simple Product</td>
                                                <td>Glasses</td>
                                                <td>ITEM-0001</td>
                                                <td>$120.00</td>
                                                <td>80,000</td>
                                                <td className="text-center"> <i className="flaticon-cart-bag-1"></i> </td>
                                                <td className="align-center">
                                                    <ul className="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <i className="flaticon-edit"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i className="flaticon-delete-5"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>0011</td>
                                                <td className="text-center">
                                                  <a className="product-list-img" href="javascript: void(0);">
                                                    <Image src="/assets/img/60x60.jpg" alt="product" height={60} width={60} />
                                                  </a>
                                                </td>
                                                <td>Flower Pot</td>
                                                <td>Simple Product</td>
                                                <td>Decor</td>
                                                <td>ITEM-0002</td>
                                                <td>$149.00</td>
                                                <td>64,000</td>
                                                <td className="text-center"> <i className="flaticon-cart-bag"></i> </td>
                                                <td className="align-center">
                                                    <ul className="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <i className="flaticon-edit"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i className="flaticon-delete-5"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>0012</td>
                                                <td className="text-center">
                                                  <a className="product-list-img" href="javascript: void(0);">
                                                    <Image src="/assets/img/60x60.jpg" alt="product" height={60} width={60} />
                                                  </a>
                                                </td>
                                                <td>Chair</td>
                                                <td>Simple Product</td>
                                                <td>Furniture</td>
                                                <td>ITEM-0003</td>
                                                <td>$49.00</td>
                                                <td>42,000</td>
                                                <td className="text-center"> <i className="flaticon-cart-bag-1"></i> </td>
                                                <td className="align-center">
                                                    <ul className="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <i className="flaticon-edit"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i className="flaticon-delete-5"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>0013</td>
                                                <td className="text-center">
                                                  <a className="product-list-img" href="javascript: void(0);">
                                                    <Image src="/assets/img/60x60.jpg" alt="product" height={60} width={60} />
                                                  </a>
                                                </td>
                                                <td>Night Lamp</td>
                                                <td>Simple Product</td>
                                                <td>Decor</td>
                                                <td>ITEM-0004</td>
                                                <td>$79.00</td>
                                                <td>51,000</td>
                                                <td className="text-center"> <i className="flaticon-cart-bag-1"></i> </td>
                                                <td className="align-center">
                                                    <ul className="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <i className="flaticon-edit"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i className="flaticon-delete-5"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>0014</td>
                                                <td className="text-center">
                                                  <a className="product-list-img" href="javascript: void(0);">
                                                    <Image src="/assets/img/60x60.jpg" alt="product" height={60} width={60} />
                                                  </a>
                                                </td>
                                                <td>Wall Clock</td>
                                                <td>Simple Product</td>
                                                <td>Electronics</td>
                                                <td>ITEM-0005</td>
                                                <td>$120.00</td>
                                                <td>80,000</td>
                                                <td className="text-center"> <i className="flaticon-cart-bag"></i> </td>
                                                <td className="align-center">
                                                    <ul className="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <i className="flaticon-edit"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i className="flaticon-delete-5"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>0015</td>
                                                <td className="text-center">
                                                  <a className="product-list-img" href="javascript: void(0);">
                                                    <Image src="/assets/img/60x60.jpg" alt="product" height={60} width={60} />
                                                  </a>
                                                </td>
                                                <td>Canon 1300B</td>
                                                <td>Simple Product</td>
                                                <td>Electronics</td>
                                                <td>ITEM-0006</td>
                                                <td>$149.00</td>
                                                <td>64,000</td>
                                                <td className="text-center"> <i className="flaticon-cart-bag"></i> </td>
                                                <td className="align-center">
                                                    <ul className="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <i className="flaticon-edit"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i className="flaticon-delete-5"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>0016</td>
                                                <td className="text-center">
                                                  <a className="product-list-img" href="javascript: void(0);">
                                                    <Image src="/assets/img/60x60.jpg" alt="product" height={60} width={60} />
                                                  </a>
                                                </td>
                                                <td>Leather Bag</td>
                                                <td>Simple Product</td>
                                                <td>Accessories</td>
                                                <td>ITEM-0007</td>
                                                <td>$49.00</td>
                                                <td>42,000</td>
                                                <td className="text-center"> <i className="flaticon-cart-bag-1"></i> </td>
                                                <td className="align-center">
                                                    <ul className="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <i className="flaticon-edit"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i className="flaticon-delete-5"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>0017</td>
                                                <td className="text-center">
                                                  <a className="product-list-img" href="javascript: void(0);">
                                                    <Image src="/assets/img/60x60.jpg" alt="product" height={60} width={60} />
                                                  </a>
                                                </td>
                                                <td>Adidas Cap</td>
                                                <td>Simple Product</td>
                                                <td>Accessories</td>
                                                <td>ITEM-0008</td>
                                                <td>$79.00</td>
                                                <td>51,000</td>
                                                <td className="text-center"> <i className="flaticon-cart-bag-1"></i> </td>
                                                <td className="align-center">
                                                    <ul className="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <i className="flaticon-edit"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i className="flaticon-delete-5"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>0018</td>
                                                <td className="text-center">
                                                  <a className="product-list-img" href="javascript: void(0);">
                                                    <Image src="/assets/img/60x60.jpg" alt="product" height={60} width={60} />
                                                  </a>
                                                </td>
                                                <td>Winter Boots</td>
                                                <td>Simple Product</td>
                                                <td>Footwear</td>
                                                <td>ITEM-0009</td>
                                                <td>$120.00</td>
                                                <td>80,000</td>
                                                <td className="text-center"> <i className="flaticon-cart-bag"></i> </td>
                                                <td className="align-center">
                                                    <ul className="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <i className="flaticon-edit"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i className="flaticon-delete-5"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>0019</td>
                                                <td className="text-center">
                                                  <a className="product-list-img" href="javascript: void(0);">
                                                    <Image src="/assets/img/60x60.jpg" alt="product" height={60} width={60} />
                                                  </a>
                                                </td>
                                                <td>Hover Board</td>
                                                <td>Simple Product</td>
                                                <td>Sports</td>
                                                <td>ITEM-0010</td>
                                                <td>$149.00</td>
                                                <td>64,000</td>
                                                <td className="align-center"> <i className="flaticon-cart-bag"></i> </td>
                                                <td className="align-center">
                                                    <ul className="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <i className="flaticon-edit"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i className="flaticon-delete-5"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
  </>
)
}