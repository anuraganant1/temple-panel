import Image from "next/image";
import { useRef, useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

export default function PlanList() {
  const [formActive, setformActive] = useState(false);
  const formikRef = useRef();
  const initialValues = {
    categoryPlan: "",
    planPeriod: "",
    planName: "",
    description: "",
    amount: "",
    status: "0",
  };

  const FormSchema = Yup.object().shape({
    categoryPlan: Yup.string().required("Select Category"),
    planPeriod: Yup.string().required("Select Period"),
    planName: Yup.string()
      .min(3, "Must be 3 characters or more")
      .max(40, "Must be 40 characters or less")
      .required("Name Required"),
    amount: Yup.string()
      .required("Amount Required"),
    description: Yup.string().required("Description Required"),
  });

  const handleSubmit = (values, { resetForm }) => {
    console.log("values", values);
    resetForm({ values: "" });
  };

  return (
    <>
      <div className="row margin-bottom-120">
        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div className="statbox widget box box-shadow">
            <div className="widget-header">
              <div className="row">
                <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                  <h4 style={{ display: "inline-block" }}>
                    {!formActive ? (
                      <span>Plan List</span>
                    ) : (
                      <span>Manage Subscriptions Plan</span>
                    )}
                  </h4>
                  {!formActive ? (
                    <button
                      style={{ display: "inline-block", float: "right" }}
                      type="button"
                      className="btn btn-success  btn-rounded mb-4 mr-2"
                      onClick={() => setformActive(true)}
                    >
                      Add Plan List
                    </button>
                  ) : (
                    <button
                      style={{ display: "inline-block", float: "right" }}
                      type="button"
                      className="btn btn-success  btn-rounded mb-4 mr-2"
                      onClick={() => setformActive(false)}
                    >
                      Close
                    </button>
                  )}

                  <div
                    className="modal fade"
                    id="registerModal"
                    role="dialog"
                    aria-labelledby="categoryModal"
                    aria-hidden="true"
                  >
                    <div className="modal-dialog modal-sm" role="document">
                      <div className="modal-content">
                        <div className="modal-header" id="categoryModal">
                          <h4 className="modal-title">Add Temple</h4>
                          <button
                            type="button"
                            className="close"
                            data-dismiss="modal"
                            aria-hidden="true"
                          >
                            ×
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {!formActive ? (
              <div className="widget-content widget-content-area">
                <div className="table-responsive mb-4">
                  <table
                    id="ecommerce-product-list"
                    className="table  table-bordered"
                  >
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Category</th>
                        <th>Sku</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th className="align-center">Status</th>
                        <th className="align-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>0010</td>
                        <td className="text-center">
                          <a
                            className="product-list-img"
                            href="javascript: void(0);"
                          >
                            <Image
                              src="/assets/img/60x60.jpg"
                              alt="product"
                              height={60}
                              width={60}
                            />
                          </a>
                        </td>
                        <td>CK Glasses</td>
                        <td>Simple Product</td>
                        <td>Glasses</td>
                        <td>ITEM-0001</td>
                        <td>$120.00</td>
                        <td>80,000</td>
                        <td className="text-center">
                          {" "}
                          <i className="flaticon-cart-bag-1"></i>{" "}
                        </td>
                        <td className="align-center">
                          <ul className="table-controls">
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Edit"
                              >
                                <i className="flaticon-edit"></i>
                              </a>
                            </li>
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                              >
                                <i className="flaticon-delete-5"></i>
                              </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td>0011</td>
                        <td className="text-center">
                          <a
                            className="product-list-img"
                            href="javascript: void(0);"
                          >
                            <Image
                              src="/assets/img/60x60.jpg"
                              alt="product"
                              height={60}
                              width={60}
                            />
                          </a>
                        </td>
                        <td>Flower Pot</td>
                        <td>Simple Product</td>
                        <td>Decor</td>
                        <td>ITEM-0002</td>
                        <td>$149.00</td>
                        <td>64,000</td>
                        <td className="text-center">
                          {" "}
                          <i className="flaticon-cart-bag"></i>{" "}
                        </td>
                        <td className="align-center">
                          <ul className="table-controls">
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Edit"
                              >
                                <i className="flaticon-edit"></i>
                              </a>
                            </li>
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                              >
                                <i className="flaticon-delete-5"></i>
                              </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td>0012</td>
                        <td className="text-center">
                          <a
                            className="product-list-img"
                            href="javascript: void(0);"
                          >
                            <Image
                              src="/assets/img/60x60.jpg"
                              alt="product"
                              height={60}
                              width={60}
                            />
                          </a>
                        </td>
                        <td>Chair</td>
                        <td>Simple Product</td>
                        <td>Furniture</td>
                        <td>ITEM-0003</td>
                        <td>$49.00</td>
                        <td>42,000</td>
                        <td className="text-center">
                          {" "}
                          <i className="flaticon-cart-bag-1"></i>{" "}
                        </td>
                        <td className="align-center">
                          <ul className="table-controls">
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Edit"
                              >
                                <i className="flaticon-edit"></i>
                              </a>
                            </li>
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                              >
                                <i className="flaticon-delete-5"></i>
                              </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td>0013</td>
                        <td className="text-center">
                          <a
                            className="product-list-img"
                            href="javascript: void(0);"
                          >
                            <Image
                              src="/assets/img/60x60.jpg"
                              alt="product"
                              height={60}
                              width={60}
                            />
                          </a>
                        </td>
                        <td>Night Lamp</td>
                        <td>Simple Product</td>
                        <td>Decor</td>
                        <td>ITEM-0004</td>
                        <td>$79.00</td>
                        <td>51,000</td>
                        <td className="text-center">
                          {" "}
                          <i className="flaticon-cart-bag-1"></i>{" "}
                        </td>
                        <td className="align-center">
                          <ul className="table-controls">
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Edit"
                              >
                                <i className="flaticon-edit"></i>
                              </a>
                            </li>
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                              >
                                <i className="flaticon-delete-5"></i>
                              </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td>0014</td>
                        <td className="text-center">
                          <a
                            className="product-list-img"
                            href="javascript: void(0);"
                          >
                            <Image
                              src="/assets/img/60x60.jpg"
                              alt="product"
                              height={60}
                              width={60}
                            />
                          </a>
                        </td>
                        <td>Wall Clock</td>
                        <td>Simple Product</td>
                        <td>Electronics</td>
                        <td>ITEM-0005</td>
                        <td>$120.00</td>
                        <td>80,000</td>
                        <td className="text-center">
                          {" "}
                          <i className="flaticon-cart-bag"></i>{" "}
                        </td>
                        <td className="align-center">
                          <ul className="table-controls">
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Edit"
                              >
                                <i className="flaticon-edit"></i>
                              </a>
                            </li>
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                              >
                                <i className="flaticon-delete-5"></i>
                              </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td>0015</td>
                        <td className="text-center">
                          <a
                            className="product-list-img"
                            href="javascript: void(0);"
                          >
                            <Image
                              src="/assets/img/60x60.jpg"
                              alt="product"
                              height={60}
                              width={60}
                            />
                          </a>
                        </td>
                        <td>Canon 1300B</td>
                        <td>Simple Product</td>
                        <td>Electronics</td>
                        <td>ITEM-0006</td>
                        <td>$149.00</td>
                        <td>64,000</td>
                        <td className="text-center">
                          {" "}
                          <i className="flaticon-cart-bag"></i>{" "}
                        </td>
                        <td className="align-center">
                          <ul className="table-controls">
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Edit"
                              >
                                <i className="flaticon-edit"></i>
                              </a>
                            </li>
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                              >
                                <i className="flaticon-delete-5"></i>
                              </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td>0016</td>
                        <td className="text-center">
                          <a
                            className="product-list-img"
                            href="javascript: void(0);"
                          >
                            <Image
                              src="/assets/img/60x60.jpg"
                              alt="product"
                              height={60}
                              width={60}
                            />
                          </a>
                        </td>
                        <td>Leather Bag</td>
                        <td>Simple Product</td>
                        <td>Accessories</td>
                        <td>ITEM-0007</td>
                        <td>$49.00</td>
                        <td>42,000</td>
                        <td className="text-center">
                          {" "}
                          <i className="flaticon-cart-bag-1"></i>{" "}
                        </td>
                        <td className="align-center">
                          <ul className="table-controls">
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Edit"
                              >
                                <i className="flaticon-edit"></i>
                              </a>
                            </li>
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                              >
                                <i className="flaticon-delete-5"></i>
                              </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td>0017</td>
                        <td className="text-center">
                          <a
                            className="product-list-img"
                            href="javascript: void(0);"
                          >
                            <Image
                              src="/assets/img/60x60.jpg"
                              alt="product"
                              height={60}
                              width={60}
                            />
                          </a>
                        </td>
                        <td>Adidas Cap</td>
                        <td>Simple Product</td>
                        <td>Accessories</td>
                        <td>ITEM-0008</td>
                        <td>$79.00</td>
                        <td>51,000</td>
                        <td className="text-center">
                          {" "}
                          <i className="flaticon-cart-bag-1"></i>{" "}
                        </td>
                        <td className="align-center">
                          <ul className="table-controls">
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Edit"
                              >
                                <i className="flaticon-edit"></i>
                              </a>
                            </li>
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                              >
                                <i className="flaticon-delete-5"></i>
                              </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td>0018</td>
                        <td className="text-center">
                          <a
                            className="product-list-img"
                            href="javascript: void(0);"
                          >
                            <Image
                              src="/assets/img/60x60.jpg"
                              alt="product"
                              height={60}
                              width={60}
                            />
                          </a>
                        </td>
                        <td>Winter Boots</td>
                        <td>Simple Product</td>
                        <td>Footwear</td>
                        <td>ITEM-0009</td>
                        <td>$120.00</td>
                        <td>80,000</td>
                        <td className="text-center">
                          {" "}
                          <i className="flaticon-cart-bag"></i>{" "}
                        </td>
                        <td className="align-center">
                          <ul className="table-controls">
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Edit"
                              >
                                <i className="flaticon-edit"></i>
                              </a>
                            </li>
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                              >
                                <i className="flaticon-delete-5"></i>
                              </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td>0019</td>
                        <td className="text-center">
                          <a
                            className="product-list-img"
                            href="javascript: void(0);"
                          >
                            <Image
                              src="/assets/img/60x60.jpg"
                              alt="product"
                              height={60}
                              width={60}
                            />
                          </a>
                        </td>
                        <td>Hover Board</td>
                        <td>Simple Product</td>
                        <td>Sports</td>
                        <td>ITEM-0010</td>
                        <td>$149.00</td>
                        <td>64,000</td>
                        <td className="align-center">
                          {" "}
                          <i className="flaticon-cart-bag"></i>{" "}
                        </td>
                        <td className="align-center">
                          <ul className="table-controls">
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Edit"
                              >
                                <i className="flaticon-edit"></i>
                              </a>
                            </li>
                            <li>
                              <a
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                              >
                                <i className="flaticon-delete-5"></i>
                              </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            ) : (
              <Formik
                innerRef={formikRef}
                initialValues={initialValues}
                validationSchema={FormSchema}
                onSubmit={handleSubmit}
              >
                {({ values }) => (
                  <Form className="form-horizontal1 form-white form-1col text-black">
                    <div className="row">
                            <div className=" col-sm-6 mb-30">
                         <label htmlFor="">Category Plan</label> 
                        <Field
                          as="select"
                          className="form-control"
                          name="categoryPlan"
                        >
                          <option value="" label="Select Category Plan">
                            Select Category Plan
                          </option>
                          <option value="CategoryA">CategoryA</option>
                          <option value="CategoryB">CategoryB</option>
                        </Field>

                        <p className="formError">
                          <ErrorMessage name="categoryPlan" />
                        </p>
                      </div>
                      <div className=" col-sm-6 mb-30">
                         <label htmlFor="">Plan Period</label> 
                        <Field
                          as="select"
                          className="form-control"
                          name="planPeriod"
                        >
                          <option value="" label="Select Plan Period">
                            Select Plan Period
                          </option>
                          <option value="PeriodA">PeriodA</option>
                          <option value="PeriodB">PeriodB</option>
                        </Field>

                        <p className="formError">
                          <ErrorMessage name="planPeriod" />
                        </p>
                      </div>
                      <div className=" col-sm-6 mb-30">
                        <label htmlFor="">Name</label> 
                        <Field
                          type="text"
                          className="form-control"
                          name="planName"
                          placeholder="Enter Name *"
                        />
                        <p className="formError">
                          <ErrorMessage name="planName" />
                        </p>
                      </div>

                       <div className=" col-sm-6 mb-30">
                        <label htmlFor="">Amount</label>
                        <Field
                          type="text"
                          className="form-control"
                          name="amount"
                          placeholder="Enter Amount *"
                        />
                        <p className="formError">
                          <ErrorMessage name="amount" />
                        </p>
                      </div>


                      <div className=" col-sm-12 mb-30">
                       <label htmlFor="">Description </label> 
                        <Field
                          type="text"
                          component="textarea"
                          rows="3"
                          className="form-control"
                          placeholder="Enter Description"
                          name="description"
                        />
                        <p className="formError">
                          <ErrorMessage name="description" />
                        </p>
                      </div>

                     
                      <div className="col-sm-6 mb-30">
                        <label htmlFor="" className="disblock">
                          Status{" "}
                        </label>
                        <label className="form-check form-check-inline">
                          <Field
                            className="form-check-input"
                            type="radio"
                            name="status"
                            value="1"
                          />
                          <span className="form-check-label">Active </span>
                        </label>
                        <label className="form-check form-check-inline">
                          <Field
                            className="form-check-input"
                            type="radio"
                            name="status"
                            value="0"
                          />
                          <span className="form-check-label">Deactive </span>
                        </label>
                      </div>

                      <div className="col-sm-12 text-center">
                        <button
                          type="submit"
                          className="btn btn-success btn-rounded"
                        >
                          Submit
                        </button>
                      </div>

                      <div className="col-sm-12 contiontext m-b"></div>
                    </div>
                  </Form>
                )}
              </Formik>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
