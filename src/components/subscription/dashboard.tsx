import { useEffect } from "react";


function Dashboard() {
  useEffect(() => {
    initMenu();
  }, []);

  const initMenu = () => {

  };

  return (
    <>
        <div className="container dashbaord-bg">
          <div className="page-header">
            <div className="page-title">
              <h3>Dashboard</h3>
            </div>
          </div>

          <div className="row layout-spacing ">
            <div className="col-xl-3 mb-xl-0 col-lg-6 mb-4 col-md-6 col-sm-6">
              <div className="widget-content-area  data-widgets br-4">
                <div className="widget  t-sales-widget">
                  <div className="media">
                    <div className="icon ml-2">
                      <i className="flaticon-line-chart"></i>
                    </div>
                    <div className="media-body text-right">
                      <p className="widget-text mb-0">Sales</p>
                      <p className="widget-numeric-value">98,225</p>
                    </div>
                  </div>
                  <p className="widget-total-stats mt-2">94% New Sales</p>
                </div>
              </div>
            </div>
            <div className="col-xl-3 mb-xl-0 col-lg-6 mb-4 col-md-6 col-sm-6">
              <div className="widget-content-area  data-widgets br-4">
                <div className="widget  t-order-widget">
                  <div className="media">
                    <div className="icon ml-2">
                      <i className="flaticon-cart-bag"></i>
                    </div>
                    <div className="media-body text-right">
                      <p className="widget-text mb-0">Orders</p>
                      <p className="widget-numeric-value">24,017</p>
                    </div>
                  </div>
                  <p className="widget-total-stats mt-2">552 New Orders</p>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-lg-6 col-md-6 col-sm-6 mb-sm-0 mb-4">
              <div className="widget-content-area  data-widgets br-4">
                <div className="widget  t-customer-widget">
                  <div className="media">
                    <div className="icon ml-2">
                      <i className="flaticon-user-11"></i>
                    </div>
                    <div className="media-body text-right">
                      <p className="widget-text mb-0">Customers</p>
                      <p className="widget-numeric-value">92,251</p>
                    </div>
                  </div>
                  <p className="widget-total-stats mt-2">390 New Customers</p>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-lg-6 col-md-6 col-sm-6">
              <div className="widget-content-area  data-widgets br-4">
                <div className="widget  t-income-widget">
                  <div className="media">
                    <div className="icon ml-2">
                      <i className="flaticon-money"></i>
                    </div>
                    <div className="media-body text-right">
                      <p className="widget-text mb-0">Income</p>
                      <p className="widget-numeric-value">9.5 M</p>
                    </div>
                  </div>
                  <p className="widget-total-stats mt-2">$2.1 M This Week</p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xl-4 col-lg-6 col-md-6 col-12 layout-spacing">
              <div className="widget-content-area chat-messages p-0  br-4">
                <div className="chat-container">
                  <div className="chat-header">
                    <div className="media">
                      <i className="flaticon-mail-fill icon mr-4"></i>
                      <div className="media-body">
                        <h6 className="">Message</h6>
                        <p className="mb-0">3 Unread Message</p>
                      </div>
                      <div className="float-right">
                        <i className="flaticon-refresh-1 js-refresh mr-1"></i>
                        <div className="dropdown custom-dropdown d-inline-block">
                          <a
                            className="dropdown-toggle pl-0"
                            href="#"
                            role="button"
                            id="dropdownMenuLink"
                            data-toggle="dropdown"
                            aria-expanded="false"
                          >
                            <i className="flaticon-dots"></i>
                          </a>
                          <div
                            className="dropdown-menu dropdown-menu-right"
                            aria-labelledby="dropdownMenuLink"
                          >
                            <a
                              className="dropdown-item"
                              href="javascript:void(0);"
                            >
                              Account
                            </a>
                            <a
                              className="dropdown-item"
                              href="javascript:void(0);"
                            >
                              Profile
                            </a>
                            <a
                              className="dropdown-item"
                              href="javascript:void(0);"
                            >
                              Settings
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="chat-body reload-widget-area">
                    <div
                      className="mCustomScrollbar message-scroll"
                      data-mcs-theme="minimal-dark"
                    >
                      <div className="table-responsive">
                        <table className="table">
                          <tbody>
                            <tr>
                              <td>
                                <div className="media">
                                  <img
                                    src="/assets/img/90x90.jpg"
                                    className="rounded-circle mr-4"
                                    alt="user"
                                    width={44}
                                    height={44}
                                  />
                                  <div className="media-body">
                                    <h6 className="usr-name">Andy King</h6>
                                    <p className="message">
                                      Hey, where have you been?
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td className="text-right">
                                <p className="meta-time">5 min</p>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div className="media">
                                  <img
                                    src="/assets/img/90x90.jpg"
                                    className="rounded-circle mr-4"
                                    alt="user"
                                    width={44}
                                    height={44}
                                  />
                                  <div className="media-body">
                                    <h6 className="usr-name">Shaun Park</h6>
                                    <p className="message">
                                      What up man? Good Morning
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td className="text-right">
                                <p className="meta-time">7 min</p>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div className="media">
                                  <img
                                    src="/assets/img/90x90.jpg"
                                    className="rounded-circle mr-4"
                                    alt="user"
                                    width={44}
                                    height={44}
                                  />
                                  <div className="media-body">
                                    <h6 className="usr-name">Nia Hillyer</h6>
                                    <p className="message">
                                      Hey, why are you not eating anything?
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td className="text-right">
                                <p className="meta-time">11 min</p>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div className="media">
                                  <img
                                    src="/assets/img/90x90.jpg"
                                    className="rounded-circle mr-4"
                                    alt="user"
                                    width={44}
                                    height={44}
                                  />
                                  <div className="media-body">
                                    <h6 className="usr-name">Mary McDonald</h6>
                                    <p className="message">
                                      I never said that for sure
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td className="text-right">
                                <p className="meta-time">20 min</p>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div className="media">
                                  <img
                                    src="/assets/img/90x90.jpg"
                                    className="rounded-circle mr-4"
                                    alt="user"
                                    width={44}
                                    height={44}
                                  />
                                  <div className="media-body">
                                    <h6 className="usr-name">Lisa Doe</h6>
                                    <p className="message">
                                      That not what I heard from Sammy
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td className="text-right">
                                <p className="meta-time">25 min</p>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div className="media">
                                  <img
                                    src="/assets/img/90x90.jpg"
                                    className="rounded-circle mr-4"
                                    alt="user"
                                    width={44}
                                    height={44}
                                  />
                                  <div className="media-body">
                                    <h6 className="usr-name">Alma Clarke</h6>
                                    <p className="message">
                                      Good Morning Friends
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td className="text-right">
                                <p className="meta-time">33 min</p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="text-center show-all-msg p-4">
                      <a href="#">All messages</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
              <div className="widget-content-area card-widget p-0  br-4">
                <div className="card-1 br-4">
                  <div className="d-flex justify-content-between mb-5">
                    <p className="card-title">Team Meeting</p>
                    <p className="meta-time">12:30 - 2:30 PM</p>
                  </div>

                  <div className="row">
                    <div className="col-md-12 mb-4">
                      <p className="card-text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua.
                      </p>
                      <p>
                        {" "}
                        Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo consequat.
                      </p>
                    </div>

                    <div className="col-md-12 text-center mt-sm-3">
                      <button className="btn btn-outline-default btn-rounded">
                        View Details
                      </button>
                    </div>
                  </div>

                  <ul className="list-inline badge-collapsed-img badge-tooltip mt-5 mb-0 text-right mr-3">
                    <li className="list-inline-item chat-online-usr">
                      <img
                        data-original-title="Alma Clarke"
                        alt="admin-profile"
                        src="/assets/img/90x90.jpg"
                        className="ml-0 bs-tooltip"
                        width={44}
                        height={44}
                      />
                    </li>
                    <li className="list-inline-item chat-online-usr">
                      <img
                        data-original-title="Alan Green"
                        alt="admin-profile"
                        src="/assets/img/90x90.jpg"
                        className="bs-tooltip"
                        width={44}
                        height={44}
                      />
                    </li>
                    <li className="list-inline-item chat-online-usr">
                      <img
                        data-original-title="Daisy Anderson"
                        alt="admin-profile"
                        src="/assets/img/90x90.jpg"
                        className="bs-tooltip"
                        width={44}
                        height={44}
                      />
                    </li>
                    <li className="list-inline-item chat-online-usr">
                      <img
                        data-original-title="Judy Holmes"
                        alt="admin-profile"
                        src="/assets/img/90x90.jpg"
                        className="bs-tooltip"
                        width={44}
                        height={44}
                      />
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div className="col-xl-4 col-lg-12 col-md-12 col-12 layout-spacing">
              <div className="statbox widget box order-summary">
                <div className="widget-header ">
                  <div className="row">
                    <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4>Order Summary</h4>
                    </div>
                  </div>
                </div>
                <div className="widget-content widget-content-area ">
                  <p className="card-title pl-2 mb-0 mt-1">Total Balance</p>
                  <div className="d-flex justify-content-between mt-4">
                    <p className="t-amount mb-2">168,500</p>
                    <p className="order-rate mt-auto">
                      20% <i className="flaticon-double-check ml-2"></i>
                    </p>
                  </div>
                  <div className="row mt-5">
                    <div className="col-sm-6 mb-sm-4 mb-2">
                      <div className="media processed">
                        <i className="flaticon-cart-bag icon mr-2"></i>
                        <div className="media-body">
                          <p className="mt-1">Processed</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 mb-sm-4 mb-2">
                      <div className="progress progress-md">
                        <div
                          className="progress-bar"
                          role="progressbar"
                          style={{ width: 85 + "%" }}
                          aria-valuenow={25}
                          aria-valuemin={0}
                          aria-valuemax={100}
                        ></div>
                      </div>
                    </div>
                    <div className="col-sm-6 mb-sm-4 mb-2">
                      <div className="media pending">
                        <i className="flaticon-danger-2 icon mr-2"></i>
                        <div className="media-body">
                          <p className="mt-1">Pending</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 mb-sm-4 mb-2">
                      <div className="progress progress-md">
                        <div
                          className="progress-bar"
                          role="progressbar"
                          style={{ width: 25 + "%" }}
                          aria-valuenow={25}
                          aria-valuemin={0}
                          aria-valuemax={100}
                        ></div>
                      </div>
                    </div>
                    <div className="col-sm-6 mb-sm-4 mb-2">
                      <div className="media delivered">
                        <i className="flaticon-gift icon mr-2"></i>
                        <div className="media-body">
                          <p className="mt-1">Delivered</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 mb-sm-4 mb-2">
                      <div className="progress progress-md">
                        <div
                          className="progress-bar"
                          role="progressbar"
                          style={{ width: 90 + "%" }}
                          aria-valuenow={25}
                          aria-valuemin={0}
                          aria-valuemax={100}
                        ></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-8 col-lg-12 col-md-12 col-12 layout-spacing">
              <div className="widget-content-area monthly-chart  br-4">
                <div className="row">
                  <div className="col-lg-4 col-md-6 col-sm-2 col-12  align-self-center">
                    <h3>Statistics</h3>
                  </div>
                  <div className="col-lg-8 col-md-6 col-sm-10 col-12 mt-sm-0 mt-3">
                    <ul
                      className="nav justify-content-sm-end justify-content-center monthly-chart-tab nav-pills"
                      id="monthly-chart"
                      role="tablist"
                    >
                      <li className="nav-item">
                        <a
                          className="nav-link active"
                          id="monthly-chart-weekly-tab"
                          data-toggle="pill"
                          href="#monthly-chart-weekly"
                          role="tab"
                          aria-controls="monthly-chart-weekly"
                          aria-selected="true"
                        >
                          Weekly
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link"
                          id="monthly-chart-monthly-tab"
                          data-toggle="pill"
                          href="#monthly-chart-monthly"
                          role="tab"
                          aria-controls="monthly-chart-monthly"
                          aria-selected="true"
                        >
                          Monthly
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link"
                          id="monthly-chart-yearly-tab"
                          data-toggle="pill"
                          href="#monthly-chart-yearly"
                          role="tab"
                          aria-controls="monthly-chart-yearly"
                          aria-selected="false"
                        >
                          Yearly
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-12 mt-3">
                    <div className="tab-content" id="monthly-chartContent">
                      <div
                        className="tab-pane fade show active"
                        id="monthly-chart-weekly"
                        role="tabpanel"
                        aria-labelledby="monthly-chart-weekly-tab"
                      >
                        <div
                          className="v-pv-weekly"
                          style={{
                            height: 300,
                            width: 100 + "%",
                            marginTop: 30,
                          }}
                        ></div>

                        <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                          <div className="row mt-3">
                            <div className="col-lg-5 col-md-5 col-sm-5 col-12 text-sm-right text-center mb-3 mr-sm-3 px-xl-0">
                              <div className="d-flex justify-content-sm-end  justify-content-center">
                                <div className="d-m-visitors data-marker align-self-center"></div>
                                <span className="visitors">
                                  Visitors : 9,823
                                </span>
                              </div>
                            </div>
                            <div className="col-lg-5 col-md-5 col-sm-5 col-12 text-sm-left text-center mb-3 ml-sm-3 px-xl-0">
                              <div className="d-flex justify-content-sm-start  justify-content-center">
                                <div className="d-m-page-view data-marker align-self-center"></div>
                                <span className="page-view">
                                  Pageviews : 21,655
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div
                        className="tab-pane fade"
                        id="monthly-chart-monthly"
                        role="tabpanel"
                        aria-labelledby="monthly-chart-monthly-tab"
                      >
                        <div
                          className="v-pv-monthly"
                          style={{
                            height: 300,
                            width: 100 + "%",
                            marginTop: 30,
                          }}
                        ></div>

                        <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                          <div className="row mt-3">
                            <div className="col-lg-5 col-md-5 col-sm-5 col-12 text-sm-right text-center mb-3 mr-sm-3 px-xl-0">
                              <div className="d-flex justify-content-sm-end  justify-content-center">
                                <div className="d-m-visitors data-marker data-marker-success align-self-center"></div>
                                <span className="visitors">
                                  Visitors : 19,823
                                </span>
                              </div>
                            </div>
                            <div className="col-lg-5 col-md-5 col-sm-5 col-12 text-sm-left text-center mb-3 ml-sm-3 px-xl-0">
                              <div className="d-flex justify-content-sm-start  justify-content-center">
                                <div className="d-m-page-view data-marker data-marker-secondary align-self-center"></div>
                                <span className="page-view">
                                  Pageviews : 61,655
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div
                        className="tab-pane fade"
                        id="monthly-chart-yearly"
                        role="tabpanel"
                        aria-labelledby="monthly-chart-yearly-tab"
                      >
                        <div
                          className="v-pv-yearly"
                          style={{
                            height: 300,
                            width: 100 + "%",
                            marginTop: 30,
                          }}
                        ></div>

                        <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                          <div className="row mt-3">
                            <div className="col-lg-5 col-md-5 col-sm-5 col-12 text-sm-right text-center mb-3 mr-sm-3 px-xl-0">
                              <div className="d-flex justify-content-sm-end  justify-content-center">
                                <div className="d-m-visitors data-marker data-marker-success align-self-center"></div>
                                <span className="visitors">
                                  Visitors : 80,823
                                </span>
                              </div>
                            </div>
                            <div className="col-lg-5 col-md-5 col-sm-5 col-12 text-sm-left text-center mb-3 ml-sm-3 px-xl-0">
                              <div className="d-flex justify-content-sm-start  justify-content-center">
                                <div className="d-m-page-view data-marker data-marker-secondary align-self-center"></div>
                                <span className="page-view">
                                  Pageviews : 1,21,655
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-4 col-lg-12 col-sm-12 col-12 layout-spacing">
              <div className="statbox widget box">
                <div className="widget-header ">
                  <div className="row">
                    <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4>Revenue and Profit</h4>
                    </div>
                  </div>
                </div>
                <div className="widget-content widget-content-area  monthly-profit-chart">
                  <div className="row">
                    <div className="col-lg-12 col-md-12">
                      <div className="row">
                        <div className="col-lg-12 col-md-12 col-12 mt-3">
                          <div
                            className="s-r mx-auto"
                            style={{
                              height: 300,
                              width: 100 + "%",
                              maxWidth: 275,
                            }}
                          ></div>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-12 col-md-12 col-12 mt-2 mb-2">
                      <div className="row mt-4">
                        <div className="col-lg-6 col-md-6 col-6 px-xl-0 text-right">
                          <div className="d-flex justify-content-end">
                            <div className="d-m-revenue data-marker align-self-center"></div>
                            <span className="page-view mr-sm-3">Revenue</span>
                          </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-6 px-xl-0 text-left">
                          <div className="d-flex justify-content-start">
                            <div className="d-m-profit data-marker align-self-center"></div>
                            <span className="page-view">Profit</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
              <div className="statbox widget box">
                <div className="widget-header ">
                  <div className="row">
                    <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4>New Products</h4>
                    </div>
                  </div>
                </div>

                <div className="widget-content-area ">
                  <div className="table-responsive new-products">
                    <table className="table">
                      <thead>
                        <tr>
                          <th className="form-check-column text-center">
                            <label
                              htmlFor="checkAll"
                              className="new-control new-checkbox new-checkbox-rounded checkbox-primary pb-2"
                            >
                              <input
                                type="checkbox"
                                id="checkAll"
                                className="new-control-input"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </th>
                          <th>Product</th>
                          <th>Type</th>
                          <th>SKU</th>
                          <th className="text-center">Quantity</th>
                          <th>Image</th>
                          <th className="text-center">Price</th>
                          <th className="text-center">Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox new-checkbox-rounded checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input chkbox"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Camera</td>
                          <td>
                            <span className="badge badge-info badge-pill">
                              Simple
                            </span>
                          </td>
                          <td>#0001</td>
                          <td className="text-center">1</td>
                          <td>
                            <img
                              src="/assets/img/90x90.jpg"
                              className="img-fluid"
                              alt="img-1"
                              style={{ borderColor: "#3862f5" }}
                              width={44}
                              height={44}
                            />
                          </td>
                          <td className="text-center">$848.95</td>
                          <td className="text-center">
                            <div className="toolbar">
                              <div className="toolbar-toggle">...</div>
                              <ul className="toolbar-dropdown animated fadeInUp table-controls list-inline">
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="View"
                                  >
                                    <i className="flaticon-view-3"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Edit"
                                  >
                                    <i className="flaticon-edit-5"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Remove"
                                  >
                                    <i className="flaticon-delete-6"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox new-checkbox-rounded checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input chkbox"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Mobile</td>
                          <td>
                            <span className="badge badge-info badge-pill">
                              Simple
                            </span>
                          </td>
                          <td>#0002</td>
                          <td className="text-center">1</td>
                          <td>
                            <img
                              src="/assets/img/90x90.jpg"
                              className="img-fluid"
                              alt="img-1"
                              style={{ borderColor: "#07e0c4" }}
                              width={44}
                              height={44}
                            />
                          </td>
                          <td className="text-center">$529.95</td>
                          <td className="text-center">
                            <div className="toolbar">
                              <div className="toolbar-toggle">...</div>
                              <ul className="toolbar-dropdown animated fadeInUp table-controls list-inline">
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="View"
                                  >
                                    <i className="flaticon-view-3"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Edit"
                                  >
                                    <i className="flaticon-edit-5"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Remove"
                                  >
                                    <i className="flaticon-delete-6"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox new-checkbox-rounded checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input chkbox"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Windows 10</td>
                          <td>
                            <span className="badge badge-success badge-pill">
                              Digital
                            </span>
                          </td>
                          <td>#0003</td>
                          <td className="text-center">3</td>
                          <td>
                            <img
                              src="/assets/img/90x90.jpg"
                              className="img-fluid"
                              alt="img-1"
                              style={{ borderColor: "#00b1f4" }}
                              width={44}
                              height={44}
                            />
                          </td>
                          <td className="text-center">$1584.00</td>
                          <td className="text-center">
                            <div className="toolbar">
                              <div className="toolbar-toggle">...</div>
                              <ul className="toolbar-dropdown animated fadeInUp table-controls list-inline">
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="View"
                                  >
                                    <i className="flaticon-view-3"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Edit"
                                  >
                                    <i className="flaticon-edit-5"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Remove"
                                  >
                                    <i className="flaticon-delete-6"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox new-checkbox-rounded checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input chkbox"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Watch</td>
                          <td>
                            <span className="badge badge-info badge-pill">
                              Simple
                            </span>
                          </td>
                          <td>#0004</td>
                          <td className="text-center">5</td>
                          <td>
                            <img
                              src="/assets/img/90x90.jpg"
                              className="img-fluid"
                              alt="img-1"
                              style={{ borderColor: "#f8538d" }}
                              width={44}
                              height={44}
                            />
                          </td>
                          <td className="text-center">$595.99</td>
                          <td className="text-center">
                            <div className="toolbar">
                              <div className="toolbar-toggle">...</div>
                              <ul className="toolbar-dropdown animated fadeInUp table-controls list-inline">
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="View"
                                  >
                                    <i className="flaticon-view-3"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Edit"
                                  >
                                    <i className="flaticon-edit-5"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Remove"
                                  >
                                    <i className="flaticon-delete-6"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox new-checkbox-rounded checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input chkbox"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Drone</td>
                          <td>
                            <span className="badge badge-info badge-pill">
                              Simple
                            </span>
                          </td>
                          <td>#0005</td>
                          <td className="text-center">1</td>{" "}
                          <td>
                            <img
                              src="/assets/img/90x90.jpg"
                              className="img-fluid"
                              alt="img-1"
                              style={{ borderColor: "#ffbb44" }}
                              width={44}
                              height={44}
                            />
                          </td>
                          <td className="text-center">$58.00</td>
                          <td className="text-center">
                            <div className="toolbar">
                              <div className="toolbar-toggle">...</div>
                              <ul className="toolbar-dropdown animated fadeInUp table-controls list-inline">
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="View"
                                  >
                                    <i className="flaticon-view-3"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Edit"
                                  >
                                    <i className="flaticon-edit-5"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Remove"
                                  >
                                    <i className="flaticon-delete-6"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox new-checkbox-rounded checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input chkbox"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Sunglasses</td>
                          <td>
                            <span className="badge badge-secondary badge-pill">
                              Bundled
                            </span>
                          </td>
                          <td>#0006</td>
                          <td className="text-center">6</td>
                          <td>
                            <img
                              src="/assets/img/90x90.jpg"
                              className="img-fluid"
                              alt="img-1"
                              style={{ borderColor: "#25d5e4" }}
                              width={44}
                              height={44}
                            />
                          </td>
                          <td className="text-center">$123.00</td>
                          <td className="text-center">
                            <div className="toolbar">
                              <div className="toolbar-toggle">...</div>
                              <ul className="toolbar-dropdown animated fadeInUp table-controls list-inline">
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="View"
                                  >
                                    <i className="flaticon-view-3"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Edit"
                                  >
                                    <i className="flaticon-edit-5"></i>
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a
                                    href="javascript:void(0);"
                                    className="bs-tooltip"
                                    data-original-title="Remove"
                                  >
                                    <i className="flaticon-delete-6"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div className="pagination-section">
                    <ul className="pagination pagination-style-1 pagination-rounded justify-content-end mt-3 mb-3">
                      <li>
                        <a href="javascript:void(0);">«</a>
                      </li>
                      <li>
                        <a href="javascript:void(0);">1</a>
                      </li>
                      <li>
                        <a href="javascript:void(0);">2</a>
                      </li>
                      <li>
                        <a href="javascript:void(0);">3</a>
                      </li>
                      <li>
                        <a href="javascript:void(0);">4</a>
                      </li>
                      <li>
                        <a href="javascript:void(0);">5</a>
                      </li>
                      <li>
                        <a href="javascript:void(0);">»</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-6 col-lg-12 col-md-12 col-12 layout-spacing">
              <div className="widget-content-area raised-tickets p-0  h-100 br-4">
                <div className=" table-header">
                  <div className="row">
                    <div className="col-6 pr-0">
                      <p className="mb-0">Tickets</p>
                    </div>
                    <div className="col-6 pl-0 text-right">
                      <p className="mb-0">Status</p>
                    </div>
                  </div>
                </div>
                <div className="table-responsive">
                  <table className="table">
                    <tbody>
                      <tr>
                        <td>
                          <div className="media">
                            <img
                              src="/assets/img/90x90.jpg"
                              className="rounded-circle mr-4"
                              alt="user"
                              width={44}
                              height={44}
                            />
                            <div className="media-body">
                              <h6 className="usr-name">Shaun Park</h6>
                              <p className="meta-info">
                                <i className="flaticon-stopwatch-1 mr-1"></i>
                                <span className="meta-date">
                                  9 Jan 2019 | Tue -{" "}
                                </span>
                                <span className="meta-time">9:00 am</span>
                              </p>
                            </div>
                          </div>
                        </td>
                        <td className="action text-right">
                          <i
                            className="flaticon-danger-2 mb-2 t-inprogress bs-tooltip"
                            data-placement="top"
                            title="Pending"
                          ></i>
                          <i
                            className="flaticon-checked-1 mb-2 bs-tooltip"
                            data-placement="top"
                            title="Success"
                          ></i>
                          <i
                            className="flaticon-cancel-circle mb-2 bs-tooltip"
                            data-placement="top"
                            title="Close"
                          ></i>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div className="media">
                            <img
                              src="/assets/img/90x90.jpg"
                              className="rounded-circle mr-4"
                              alt="user"
                              width={44}
                              height={44}
                            />
                            <div className="media-body">
                              <h6 className="usr-name">Nia Hillyer</h6>
                              <p className="meta-info">
                                <i className="flaticon-stopwatch-1 mr-1"></i>
                                <span className="meta-date">
                                  5 Jan 2019 | Mon -{" "}
                                </span>
                                <span className="meta-time">2:00 pm</span>
                              </p>
                            </div>
                          </div>
                        </td>
                        <td className="action text-right">
                          <i
                            className="flaticon-danger-2 mb-2 bs-tooltip"
                            data-placement="top"
                            title="Pending"
                          ></i>
                          <i
                            className="flaticon-checked-1 mb-2 t-solved bs-tooltip"
                            data-placement="top"
                            title="Success"
                          ></i>
                          <i
                            className="flaticon-cancel-circle mb-2 bs-tooltip"
                            data-placement="top"
                            title="Close"
                          ></i>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div className="media">
                            <img
                              src="/assets/img/90x90.jpg"
                              className="rounded-circle mr-4"
                              alt="user"
                              width={44}
                              height={44}
                            />
                            <div className="media-body">
                              <h6 className="usr-name">Mary McDonald</h6>
                              <p className="meta-info">
                                <i className="flaticon-stopwatch-1 mr-1"></i>
                                <span className="meta-date">
                                  3 Jan 2019 | Mon -{" "}
                                </span>
                                <span className="meta-time">10:00 am</span>
                              </p>
                            </div>
                          </div>
                        </td>
                        <td className="action text-right">
                          <i
                            className="flaticon-danger-2 mb-2 bs-tooltip"
                            data-placement="top"
                            title="Pending"
                          ></i>
                          <i
                            className="flaticon-checked-1 mb-2 bs-tooltip"
                            data-placement="top"
                            title="Success"
                          ></i>
                          <i
                            className="flaticon-cancel-circle mb-2 t-not-solved bs-tooltip"
                            data-placement="top"
                            title="Close"
                          ></i>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div className="media">
                            <img
                              src="/assets/img/90x90.jpg"
                              className="rounded-circle mr-4"
                              alt="user"
                              width={44}
                              height={44}
                            />
                            <div className="media-body">
                              <h6 className="usr-name">Andy King</h6>
                              <p className="meta-info">
                                <i className="flaticon-stopwatch-1 mr-1"></i>
                                <span className="meta-date">
                                  29 Dec 2018 | Fri -{" "}
                                </span>
                                <span className="meta-time">03:00 pm</span>
                              </p>
                            </div>
                          </div>
                        </td>
                        <td className="action text-right">
                          <i
                            className="flaticon-danger-2 mb-2 bs-tooltip"
                            data-placement="top"
                            title="Pending"
                          ></i>
                          <i
                            className="flaticon-checked-1 mb-2 t-solved bs-tooltip"
                            data-placement="top"
                            title="Success"
                          ></i>
                          <i
                            className="flaticon-cancel-circle mb-2 bs-tooltip"
                            data-placement="top"
                            title="Close"
                          ></i>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <div className="col-xl-6 col-lg-12 col-md-12 col-12 layout-spacing">
              <div className="widget-content-area task-container  p-0 h-100 br-4">
                <div className="task-list">
                  <div className="task-header">
                    <div className="row">
                      <div className="col-md-5 col-sm-5 mb-4 mb-sm-0">
                        <h6 className="mt-3 mb-0">Today Task</h6>
                      </div>
                      <div className="col-md-7 col-sm-7 text-sm-right">
                        <button className="btn btn-gradient-warning btn-rounded">
                          Create Task
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="task-body">
                    <div className="table-responsive">
                      <table className="table">
                        <tbody>
                          <tr>
                            <td>
                              <div className="task-item">
                                <h6 className="task-title mb-3">
                                  Metting Scheduled with
                                </h6>
                                <ul className="list-inline">
                                  <li className="list-inline-item">
                                    <img
                                      src="/assets/img/90x90.jpg"
                                      className="rounded-circle mb-2"
                                      alt="user"
                                      width={44}
                                      height={44}
                                    />
                                  </li>
                                  <li className="list-inline-item">
                                    <img
                                      src="/assets/img/90x90.jpg"
                                      className="rounded-circle mb-2"
                                      alt="user"
                                      width={44}
                                      height={44}
                                    />
                                  </li>
                                  <li className="list-inline-item">
                                    <img
                                      src="/assets/img/90x90.jpg"
                                      className="rounded-circle mb-2"
                                      alt="user"
                                      width={44}
                                      height={44}
                                    />
                                  </li>
                                  <li className="list-inline-item">
                                    <img
                                      src="/assets/img/90x90.jpg"
                                      className="rounded-circle mb-2"
                                      alt="user"
                                      width={44}
                                      height={44}
                                    />
                                  </li>
                                </ul>
                              </div>
                            </td>
                            <td className="action text-right">
                              <i
                                className="flaticon-edit-fill bs-tooltip"
                                data-placement="top"
                                title="Edit"
                              ></i>
                              <i
                                className="flaticon-delete-can-fill-2 ml-2 bs-tooltip"
                                data-placement="top"
                                title="Delete"
                              ></i>
                              <br />
                              <p className="meta-info">
                                <span className="meta-time">03:20 PM Wed,</span>
                                <span className="meta-date">16 Jan 2019</span>
                              </p>
                            </td>
                          </tr>

                          <tr>
                            <td>
                              <div className="task-item">
                                <h6 className="task-title mb-3">
                                  Give purchase report to
                                </h6>
                                <ul className="list-inline">
                                  <li className="list-inline-item">
                                    <img
                                      src="/assets/img/90x90.jpg"
                                      className="rounded-circle mb-1"
                                      alt="user"
                                      width={44}
                                      height={44}
                                    />
                                  </li>
                                  <li className="list-inline-item">
                                    <div className="txt-profile txt-profile-success mb-1">
                                      G
                                    </div>
                                  </li>
                                </ul>
                              </div>
                            </td>
                            <td className="action text-right">
                              <i
                                className="flaticon-edit-fill bs-tooltip"
                                data-placement="top"
                                title="Edit"
                              ></i>
                              <i
                                className="flaticon-delete-can-fill-2 ml-2 bs-tooltip"
                                data-placement="top"
                                title="Delete"
                              ></i>
                              <br />
                              <p className="meta-info">
                                <span className="meta-time">11:00 AM Wed,</span>
                                <span className="meta-date">16 Jan 2019</span>
                              </p>
                            </td>
                          </tr>

                          <tr>
                            <td>
                              <div className="task-item">
                                <h6 className="task-title mb-3">
                                  Receive Shipment
                                </h6>
                                <ul className="list-inline">
                                  <li className="list-inline-item">
                                    <img
                                      src="/assets/img/90x90.jpg"
                                      className="rounded-circle mb-2"
                                      alt="user"
                                      width={44}
                                      height={44}
                                    />
                                  </li>
                                  <li className="list-inline-item">
                                    <img
                                      src="/assets/img/90x90.jpg"
                                      className="rounded-circle mb-2"
                                      alt="user"
                                      width={44}
                                      height={44}
                                    />
                                  </li>
                                  <li className="list-inline-item">
                                    <img
                                      src="/assets/img/90x90.jpg"
                                      className="rounded-circle mb-2"
                                      alt="user"
                                      width={44}
                                      height={44}
                                    />
                                  </li>
                                  <li className="list-inline-item">
                                    <img
                                      src="/assets/img/90x90.jpg"
                                      className="rounded-circle mb-2"
                                      alt="user"
                                      width={44}
                                      height={44}
                                    />
                                  </li>
                                </ul>
                              </div>
                            </td>
                            <td className="action text-right">
                              <i
                                className="flaticon-edit-fill bs-tooltip"
                                data-placement="top"
                                title="Edit"
                              ></i>
                              <i
                                className="flaticon-delete-can-fill-2 ml-2 bs-tooltip"
                                data-placement="top"
                                title="Delete"
                              ></i>
                              <br />
                              <p className="meta-info">
                                <span className="meta-time">09:00 AM Wed,</span>
                                <span className="meta-date">16 Jan 2019</span>
                              </p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div className="text-center action-button">
                      <button className="btn btn-light-success mb-4 mt-3 box-shadow-none">
                        View All Tasks
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-6 col-lg-12 col-md-12 col-12 layout-spacing">
              <div className="widget-content-area event-calendar p-0  h-100 br-4">
                <div className="calendar"></div>
              </div>
            </div>

            <div className="col-xl-6 col-lg-12 col-md-12 col-12 layout-spacing">
              <div className="top-searches widget-content-area p-0 widget-content-container  h-100 br-4">
                <div className="col-lg-12 col-md-12 col-12 map-title">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-6">
                      <p className="mb-4 mt-2">Top Searches</p>
                    </div>
                  </div>
                </div>
                <div className="child-content">
                  <div
                    id="world-map"
                    style={{ height: 435, maxHeight: 100 + "%" }}
                  ></div>
                  <div className="world-map-section">
                    <div className="">
                      <div className="table-responsive top-search-scroll">
                        <table className="table table-highlight-head">
                          <thead>
                            <tr>
                              <th className="align-center">
                                <div className="d-flex justify-content-center">
                                  <div className="d-m-data-1 data-marker align-self-center"></div>
                                  <span className="page-view mr-sm-3">USA</span>
                                </div>
                              </th>
                              <th className="align-center">
                                <div className="d-flex justify-content-center">
                                  <div className="d-m-data-2 data-marker align-self-center"></div>
                                  <span className="page-view mr-sm-3">
                                    Australia
                                  </span>
                                </div>
                              </th>
                              <th className="align-center">
                                <div className="d-flex justify-content-center">
                                  <div className="d-m-data-3 data-marker align-self-center"></div>
                                  <span className="page-view mr-sm-3">
                                    Spain
                                  </span>
                                </div>
                              </th>
                              <th className="align-center">
                                <div className="d-flex justify-content-center">
                                  <div className="d-m-data-4 data-marker align-self-center"></div>
                                  <span className="page-view mr-sm-3">
                                    France
                                  </span>
                                </div>
                              </th>
                              <th className="align-center">
                                <div className="d-flex justify-content-center">
                                  <div className="d-m-data-5 data-marker align-self-center"></div>
                                  <span className="page-view mr-sm-3">
                                    India
                                  </span>
                                </div>
                              </th>
                              <th className="align-center">
                                <div className="d-flex justify-content-center">
                                  <div className="d-m-data-6 data-marker align-self-center"></div>
                                  <span className="page-view mr-sm-3">
                                    Other
                                  </span>
                                </div>
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td className="align-center data-value-1">55%</td>
                              <td className="align-center data-value-2">30%</td>
                              <td className="align-center data-value-3">10%</td>
                              <td className="align-center data-value-4">4%</td>
                              <td className="align-center data-value-5">
                                0.6%
                              </td>
                              <td className="align-center data-value-6">
                                0.4%
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-8 col-lg-8 col-md-8 col-12 layout-spacing">
              <div className="statbox widget box box-shadow latest-invoice ">
                <div className="widget-header ">
                  <div className="row">
                    <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4>Latest Invoice</h4>
                    </div>
                  </div>
                </div>
                <div className="widget-content-area ">
                  <div className="table-responsive">
                    <table className="table">
                      <thead>
                        <tr>
                          <th className="form-check-column text-center">
                            <label
                              htmlFor="invoiceAll"
                              className="new-control new-checkbox checkbox-primary pb-2"
                            >
                              <input
                                type="checkbox"
                                id="invoiceAll"
                                className="new-control-input"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </th>
                          <th>Product</th>
                          <th>Invoice no.</th>
                          <th>Date</th>
                          <th className="text-center">Amount</th>
                          <th className="text-center">Status</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input invoicechk"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Camera</td>
                          <td>#0001</td>
                          <td>16 Jan</td>
                          <td className="align-center">$15,202</td>
                          <td className="text-center">
                            <span className="badge badge-pills outline-badge-secondary">
                              In Progress
                            </span>
                          </td>
                          <td>
                            <i
                              className="flaticon-view-1 bs-tooltip"
                              data-placement="top"
                              data-original-title="View"
                            ></i>
                          </td>
                        </tr>

                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input invoicechk"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Iphone</td>
                          <td>#0002</td>
                          <td>16 Jan</td>
                          <td className="align-center">$650</td>
                          <td className="text-center">
                            <span className="badge badge-pills outline-badge-success">
                              Approved
                            </span>
                          </td>
                          <td>
                            <i
                              className="flaticon-view-1 bs-tooltip"
                              data-placement="top"
                              data-original-title="View"
                            ></i>
                          </td>
                        </tr>

                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input invoicechk"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Headphone</td>
                          <td>#0003</td>
                          <td>16 Jan</td>
                          <td className="align-center">$25</td>
                          <td className="text-center">
                            <span className="badge badge-pills outline-badge-warning">
                              Pending
                            </span>
                          </td>
                          <td>
                            <i
                              className="flaticon-view-1 bs-tooltip"
                              data-placement="top"
                              data-original-title="View"
                            ></i>
                          </td>
                        </tr>

                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input invoicechk"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Speakers</td>
                          <td>#0004</td>
                          <td>16 Jan</td>
                          <td className="align-center">$100</td>
                          <td className="text-center">
                            <span className="badge badge-pills outline-badge-secondary">
                              In Progress
                            </span>
                          </td>
                          <td>
                            <i
                              className="flaticon-view-1 bs-tooltip"
                              data-placement="top"
                              data-original-title="View"
                            ></i>
                          </td>
                        </tr>

                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input invoicechk"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Watch</td>
                          <td>#0005</td>
                          <td>16 Jan</td>
                          <td className="align-center">$85</td>
                          <td className="text-center">
                            <span className="badge badge-pills outline-badge-success">
                              Approved
                            </span>
                          </td>
                          <td>
                            <i
                              className="flaticon-view-1 bs-tooltip"
                              data-placement="top"
                              data-original-title="View"
                            ></i>
                          </td>
                        </tr>

                        <tr>
                          <td className="form-check-column text-center">
                            <label className="new-control new-checkbox checkbox-primary pb-2">
                              <input
                                type="checkbox"
                                className="new-control-input invoicechk"
                              />
                              <span className="new-control-indicator mt-2"></span>
                              <span className="invisible">s</span>
                            </label>
                          </td>
                          <td>Sunglasses</td>
                          <td>#0006</td>
                          <td>16 Jan</td>
                          <td className="align-center">$19</td>
                          <td className="text-center">
                            <span className="badge badge-pills outline-badge-danger">
                              Cancelled
                            </span>
                          </td>
                          <td>
                            <i
                              className="flaticon-view-1 bs-tooltip"
                              data-placement="top"
                              data-original-title="View"
                            ></i>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-4 col-lg-4 col-md-4 col-12 layout-spacing">
              <div className="widget-content-area p-0 card-widget-content ">
                <div id="user-profile-card-1" className="card br-4">
                  <div className="card-body p-0">
                    <div className="usr-img-meta mx-auto">
                      <img
                        alt="admin-profile"
                        src="/assets/img/120x120.jpg"
                        className="rounded-circle"
                        width={44}
                        height={44}
                      />
                    </div>
                    <div className="usr-info-meta text-center">
                      <p className="usr-name mb-0">Sean Freeman</p>
                      <p className="usr-occupation">Designer</p>
                      <button className="btn btn-success btn-rounded">
                        View Profile
                      </button>
                    </div>
                    <div className="row mt-5">
                      <div className="col-lg-12 text-center mt-4">
                        <button className="btn btn-light-info rounded-circle mb-2 mr-2">
                          <i className="flaticon-twitter-logo flaticon-circle-p"></i>
                        </button>
                        <button className="btn btn-light-success rounded-circle mb-2 mr-2">
                          <i className="flaticon-behance-logo flaticon-circle-p"></i>
                        </button>
                        <button className="btn btn-light-primary rounded-circle mb-2 mr-2">
                          <i className="flaticon-facebook-logo flaticon-circle-p"></i>
                        </button>
                        <button className="btn btn-light-warning rounded-circle mb-2 mr-2">
                          <i className="flaticon-dribbble-bold flaticon-circle-p"></i>
                        </button>
                        <button className="btn btn-light-danger rounded-circle mb-2 mr-2">
                          <i className="flaticon-youtube-logo flaticon-circle-p"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-xl-5 col-lg-12 col-md-12 col-12 layout-spacing">
              <div className="widget-content-area page-views p-0  br-4">
                <ul
                  className="nav nav-pills py-3"
                  id="pills-tab"
                  role="tablist"
                >
                  <li className="nav-item">
                    <a
                      className="nav-link active"
                      id="pills-home-tab"
                      data-toggle="pill"
                      href="#pills-home"
                      role="tab"
                      aria-controls="pills-home"
                      aria-selected="true"
                    >
                      Daily
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      id="pills-profile-tab"
                      data-toggle="pill"
                      href="#pills-profile"
                      role="tab"
                      aria-controls="pills-profile"
                      aria-selected="false"
                    >
                      Weekly
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      id="pills-contact-tab"
                      data-toggle="pill"
                      href="#pills-contact"
                      role="tab"
                      aria-controls="pills-contact"
                      aria-selected="false"
                    >
                      Monthly
                    </a>
                  </li>
                </ul>
                <div className="tab-content" id="pills-tabContent">
                  <div
                    className="tab-pane fade show active"
                    id="pills-home"
                    role="tabpanel"
                    aria-labelledby="pills-home-tab"
                  >
                    <div className="row">
                      <div className="col-md-6 col-sm-6 text-center">
                        <div className="daily">
                          <p className="d-count mb-0">5,067</p>
                          <p>Total Page Views</p>
                        </div>
                      </div>
                      <div className="col-md-6 col-sm-6">
                        <div id="daily"></div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="pills-profile"
                    role="tabpanel"
                    aria-labelledby="pills-profile-tab"
                  >
                    <div className="row">
                      <div className="col-md-6 col-sm-6 text-center">
                        <div className="weekly">
                          <p className="w-count mb-0">25,067</p>
                          <p>Total Page Views</p>
                        </div>
                      </div>
                      <div className="col-md-6 col-sm-6">
                        <div id="weekly"></div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="pills-contact"
                    role="tabpanel"
                    aria-labelledby="pills-contact-tab"
                  >
                    <div className="row">
                      <div className="col-md-6 col-sm-6 text-center">
                        <div className="month">
                          <p className="m-count mb-0">276,097</p>
                          <p>Total Page Views</p>
                        </div>
                      </div>
                      <div className="col-md-6 col-sm-6">
                        <div id="month"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-7 col-lg-12 col-md-12 col-12 layout-spacing">
              <div className="row">
                <div className="col-sm-4 col-12 mb-sm-0 mb-4">
                  <div className="widget-content-area social-likes text-center p-0  br-4">
                    <div className="card facebook">
                      <div className="icon mb-4">
                        <i className="flaticon-facebook-logo"></i>
                      </div>
                      <div className="card-content">
                        <h5>Facebook</h5>
                        <p>13K Followers</p>
                      </div>
                      <div className="card-btn-section">
                        <p>View Profile</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-4 col-12 mb-sm-0 mb-4">
                  <div className="widget-content-area social-likes text-center p-0  br-4">
                    <div className="card dribbble">
                      <div className="icon mb-4">
                        <i className="flaticon-dribbble-bold"></i>
                      </div>
                      <div className="card-content">
                        <h5>Dribbble</h5>
                        <p>4K Followers</p>
                      </div>
                      <div className="card-btn-section">
                        <p>Check Work</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-4 col-12 mb-sm-0 mb-4">
                  <div className="widget-content-area social-likes text-center p-0  br-4">
                    <div className="card twitter">
                      <div className="icon mb-4">
                        <i className="flaticon-twitter-logo"></i>
                      </div>
                      <div className="card-content">
                        <h5>Twitter</h5>
                        <p>7.2K Followers</p>
                      </div>
                      <div className="card-btn-section">
                        <p>Read Tweets</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </>
  );
}

export default Dashboard;
