import { useEffect } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

// interface Props {
//   itemList: any;
//   type:any;
//   func: any;
// }

function DrapDrop(props: any) {
  console.log("props", props);
  let itemList: any;
  itemList = props["itemList"];
  const type = props["type"];
  console.log("itemList", itemList);

  useEffect(() => {}, []);

  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const grid = 8;

  const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",
    padding: grid * 2,
    margin: `0 ${grid}px ${grid}px 0`,

    // change background colour if dragging
    background: isDragging ? "lightgreen" : "#ededed",

    // styles we need to apply on draggables
    ...draggableStyle,
  });

  const getListStyle = (isDraggingOver) => ({
    background: isDraggingOver ? "white" : "white",
    padding: grid,
  });
  
  const getCardStyle = (isDraggingOver) => ({
    background: isDraggingOver ? "white" : "white",
    display: "flex",
    padding: grid,
  });

  const onDragEnd = (result) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items: any = reorder(
      itemList,
      result.source.index,
      result.destination.index
    );
    itemList = items;
    console.log("itemlist", items);
    props.func(items);
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="droppable" direction={type == "menu" ? 'horizontal' : 'vertical'}>
        {(provided, snapshot) => (
          <div
            {...provided.droppableProps}
            ref={provided.innerRef}
            style={
              type == "menu"
                ? getCardStyle(snapshot.isDraggingOver)
                : getListStyle(snapshot.isDraggingOver)
            }
          >
            {itemList.map((item, index) => (
              <Draggable
                key={type == "menu" ? item.menuId : item.pageId}
                draggableId={type == "menu" ? item.menuId : item.pageId}
                index={index}
              >
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    style={getItemStyle(
                      snapshot.isDragging,
                      provided.draggableProps.style
                    )}
                  >
                    {type == "menu" ? item.menuName : item.pageName}
                  </div>
                )}
              </Draggable>
            ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export default DrapDrop;
