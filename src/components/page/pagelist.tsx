import { useEffect, useState } from "react";
import ISKForm from "../../components/ISKForm";
import { pageSchema } from "../../services/formSchema/pageSchema";

export default function PageList() {
  const [formActive, setformActive] = useState(false);
  const [pageList, setPageList] = useState([]);
  const [flag, setFlag] = useState(0);
  let page: any = [];

  useEffect(() => {
    if (JSON.parse(localStorage.getItem("pageList")) != null) {
      page = JSON.parse(localStorage.getItem("pageList"));
      console.log(page);
      setPageList(page);
    }
  }, []);

  const pulldata = (fields) => {
    const param = fields;
    param["id"] = (pageList.length + 100).toString();
    pageList.push(param);
    setPageList(pageList);
    localStorage.setItem("pageList", JSON.stringify(pageList));
    setformActive(false);
  };

  const close = () => {
    setFlag(0);
    setformActive(false);
  };
  const edit = () => {
    setFlag(1);
  };
  return (
    <>
      <div className="row margin-bottom-120">
        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div className="statbox widget box box-shadow">
            <div className="widget-header">
              <div className="row">
                <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                  <h4 style={{ display: "inline-block" }}>
                    {!formActive ? <span>Page List</span> : <span>Page</span>}
                  </h4>
                  {!formActive && flag == 0 ? (
                    <button
                      style={{ display: "inline-block", float: "right" }}
                      type="button"
                      className="btn btn-success  btn-rounded mb-4 mr-2"
                      onClick={() => setformActive(true)}
                    >
                      Add Page
                    </button>
                  ) : (
                    <button
                      style={{ display: "inline-block", float: "right" }}
                      type="button"
                      className="btn btn-success  btn-rounded mb-4 mr-2"
                      onClick={close}
                    >
                      Close
                    </button>
                  )}
                  <div
                    className="modal fade"
                    id="registerModal"
                    role="dialog"
                    aria-labelledby="categoryModal"
                    aria-hidden="true"
                  >
                    <div className="modal-dialog modal-sm" role="document">
                      <div className="modal-content">
                        <div className="modal-header" id="categoryModal">
                          <h4 className="modal-title">Add Temple</h4>
                          <button
                            type="button"
                            className="close"
                            data-dismiss="modal"
                            aria-hidden="true"
                          >
                            ×
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {flag == 0 ? (
              !formActive ? (
                <div className="widget-content widget-content-area">
                  <div className="table-responsive mb-4">
                    <table
                      id="ecommerce-product-list"
                      className="table  table-bordered"
                    >
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Name</th>
                          <th>Priority</th>
                          <th className="align-center">Status</th>
                          <th className="align-center">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {pageList.map((item, index) => {
                          return (
                            <tr key={index}>
                              <td>{item.pageId}</td>
                              <td>{item.pageName}</td>
                              <td>{item.priority}</td>
                              <td className="text-center">{item.status}</td>
                              <td className="align-center">
                                <ul className="table-controls">
                                  <li>
                                    <a
                                      onClick={edit}
                                      data-toggle="tooltip"
                                      data-placement="top"
                                      title="Edit"
                                    >
                                      <i className="flaticon-edit"></i>
                                    </a>
                                  </li>
                                  {/* <li>
                                <a
                                  href="javascript:void(0);"
                                  data-toggle="tooltip"
                                  data-placement="top"
                                  title="Delete"
                                >
                                  <i className="flaticon-delete-5"></i>
                                </a>
                              </li> */}
                                </ul>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              ) : (
                <ISKForm
                  formSchema={pageSchema}
                  formSchemaValue={{}}
                  func={pulldata}
                />
              )
            ) : (
              <div className="statbox widget box box-shadow">
                <div className="widget-header">
                  <div className="row">
                    <div className="col-xl-12 col-md-12 col-sm-12 col-12 layout-spacing">
                      <div className="statbox widget box box-shadow">
                        <div className="widget-content widget-content-area animated-underline-content">
                          <ul
                            className="nav nav-tabs  mb-3"
                            id="animateLine"
                            role="tablist"
                          >
                            <li className="nav-item">
                              <a
                                className="nav-link active"
                                id="animated-underline-home-tab"
                                data-toggle="tab"
                                href="#animated-underline-home"
                                role="tab"
                                aria-controls="animated-underline-home"
                                aria-selected="true"
                              >
                                <i className="flaticon-home-fill-1"></i> Home
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link"
                                id="animated-underline-profile-tab"
                                data-toggle="tab"
                                href="#animated-underline-profile"
                                role="tab"
                                aria-controls="animated-underline-profile"
                                aria-selected="false"
                              >
                                <i className="flaticon-user-7"></i> Profile
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link"
                                id="animated-underline-contact-tab"
                                data-toggle="tab"
                                href="#animated-underline-contact"
                                role="tab"
                                aria-controls="animated-underline-contact"
                                aria-selected="false"
                              >
                                <i className="flaticon-telephone"></i> Contact
                              </a>
                            </li>
                          </ul>

                          <div
                            className="tab-content"
                            id="animateLineContent-4"
                          >
                            <div
                              className="tab-pane fade show active"
                              id="animated-underline-home"
                              role="tabpanel"
                              aria-labelledby="animated-underline-home-tab"
                            >
                              <ISKForm
                                formSchema={pageSchema}
                                formSchemaValue={{}}
                                func={pulldata}
                              />
                            </div>
                            <div
                              className="tab-pane fade"
                              id="animated-underline-profile"
                              role="tabpanel"
                              aria-labelledby="animated-underline-profile-tab"
                            >
                              <div className="media mt-4 mb-3">
                                <div className="media-body">
                                  Cras sit amet nibh libero, in gravida nulla.
                                  Nulla vel metus scelerisque ante sollicitudin.
                                  Cras purus odio, vestibulum in vulputate at,
                                  tempus viverra turpis. Fusce condimentum nunc
                                  ac nisi vulputate fringilla. Donec lacinia
                                  congue felis in faucibus.
                                </div>
                              </div>
                            </div>
                            <div
                              className="tab-pane fade"
                              id="animated-underline-contact"
                              role="tabpanel"
                              aria-labelledby="animated-underline-contact-tab"
                            >
                              <p className="dropcap  dc-outline-primary">
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint
                                occaecat cupidatat non proident, sunt in culpa
                                qui officia deserunt mollit anim id est laborum.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
