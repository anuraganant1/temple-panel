import { useEffect, useState } from "react";
import ISKForm from "../../components/ISKForm";
import { menuSchema } from "../../services/formSchema/menuSchema";
// import DrapDrop from "../drapdrop";
// import DoubleDrapDrop from "../doubledrapdrop";
import DragList from "../draglist";
import MenuDnd from "../menudnd";

export default function Menu() {
  const [loaded, setLoaded] = useState(false);
  const [formActive, setformActive] = useState(false);
  const [menuList, setMenuList] = useState([]);
  const [pageList, setPageList] = useState([]);
  const [dragdropList, setDragDropList] = useState([]);
  const [menuArray, setMenuArray] = useState([]);

  // let menu: any = [];
  // let page: any = [];

  useEffect(() => {
    // if (JSON.parse(localStorage.getItem("menuList")) != null) {
    //   menu = JSON.parse(localStorage.getItem("menuList"));
    //   console.log("menuList", JSON.stringify(menu));
    //   setMenuList(menu);
    //   setDragList(menu);
    // }
    // if (JSON.parse(localStorage.getItem("pageList")) != null) {
    //   page = JSON.parse(localStorage.getItem("pageList"));
    //   console.log("pageList", JSON.stringify(page));
    //   setPageList(page);
    // }

    const menu = [
      {
        menuName: "Donate",
        priority: 1,
        status: "1",
        pageList: [
          { pageName: "Page 1", prefix: "Donate", pageId: "101" },
          { pageName: "Page 2", prefix: "Donate", pageId: "102" },
        ],
        menuId: "100",
      },
      {
        menuName: "About",
        priority: 2,
        status: "1",
        pageList: [
          { pageName: "Page 1", prefix: "Donate", pageId: "101" },
          { pageName: "Page 2", prefix: "Donate", pageId: "102" },
        ],
        menuId: "101",
      }
    ];
    const page = [
      { pageName: "Page 1", priority: 1, status: "1", pageId: "100" },
      { pageName: "Page 2", priority: 2, status: "1", pageId: "101" },
      { pageName: "Page 3", priority: 3, status: "1", pageId: "102" },
      { pageName: "Page 4", priority: 4, status: "1", pageId: "103" },
    ];

    setMenuList(menu);
    setDragList(menu);
    setPageList(page);
    setLoaded(true)
  }, []);

  const pulldata = (fields) => {
    const param = fields;
    param["pageList"] = [];
    param["menuId"] = (menuList.length + 100).toString();
    menuList.push(param);
    setMenuList(menuList);
    setDragList(menuList);
    localStorage.setItem("menuList", JSON.stringify(menuList));
    setformActive(false);
  };

  const setDragList = (menu) => {
    const dragdropObj: any = {};
    const menuTmp: any = [];
    for (const i in menu) {
      if (menu[i]) {
        menuTmp.push(menu[i]["menuName"]);
        dragdropObj[menu[i]["menuName"]] = menu[i]["pageList"];
      }
    }
    setMenuArray(menuTmp)
    setDragDropList(dragdropObj);
  };

  // const reorderData = (data) => {
  //   console.log("reorderData", data); // LOGS DATA FROM CHILD (My name is Dean Winchester... &)
  // };

  if(!loaded){
    return
  }
  return (
    <>
      <div className="row margin-bottom-120">
        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div className="statbox widget box box-shadow">
            <div className="widget-header">
              <div className="row">
                <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                  <h4 style={{ display: "inline-block" }}>
                    {!formActive ? <span>Menu List</span> : <span>Menu</span>}
                  </h4>
                  {!formActive ? (
                    <button
                      style={{ display: "inline-block", float: "right" }}
                      type="button"
                      className="btn btn-success  btn-rounded mb-4 mr-2"
                      onClick={() => setformActive(true)}
                    >
                      Add Menu
                    </button>
                  ) : (
                    <button
                      style={{ display: "inline-block", float: "right" }}
                      type="button"
                      className="btn btn-success  btn-rounded mb-4 mr-2"
                      onClick={() => setformActive(false)}
                    >
                      Close
                    </button>
                  )}

                  <div
                    className="modal fade"
                    id="registerModal"
                    role="dialog"
                    aria-labelledby="categoryModal"
                    aria-hidden="true"
                  >
                    <div className="modal-dialog modal-sm" role="document">
                      <div className="modal-content">
                        <div className="modal-header" id="categoryModal">
                          <h4 className="modal-title">Add Temple</h4>
                          <button
                            type="button"
                            className="close"
                            data-dismiss="modal"
                            aria-hidden="true"
                          >
                            ×
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {!formActive ? (
              <div className="container dashbaord-bg">
                <div className="page-header">
                  <div className="page-title">
                    <h3>&nbsp;</h3>
                  </div>
                </div>

                <div className="row layout-spacing ">
                  <div className="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
                    <MenuDnd/>
                  </div>
                  <div className="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
                    <DragList
                      pageList={pageList}
                      dragList={dragdropList}
                      menuArray={menuArray}
                    />
                  </div>
                </div>
              </div>
            ) : (
              <ISKForm
                formSchema={menuSchema}
                formSchemaValue={{}}
                func={pulldata}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
}
