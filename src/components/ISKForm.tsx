import React from "react";
import { Formik, Form, Field, FieldArray, ErrorMessage } from "formik";
import * as Yup from "yup";

interface Props {
  formSchema: object;
  formSchemaValue: object;
  func:any;
}
export default function ISKForm(props: Props) { 
  const { formSchema } = props;
  console.log("formSchema", formSchema);
  let valdationSchema = null;
  const initialValues = {};
  const yupObj: any = {};
  const schemaField: any = formSchema["field"];
  // eslint-disable-next-line guard-for-in
  for (const i in schemaField) {
    initialValues[schemaField[i]["name"]] = schemaField[i]["value"];
    if (schemaField[i]["validation"] && schemaField[i]["validation"] != "") {
      yupObj[schemaField[i]["name"]] = getValidateProperty(
        schemaField[i]["validation"],
        schemaField[i]["title"]
      );
    }
  }

  const FormSchema = Yup.object().shape(yupObj);

  function onSubmit(fields) {
   props.func(fields);  
  }

  function getValidateProperty(type, title) {
    switch (type) {
      case "name":
        valdationSchema = Yup.string()
          .min(3, "Must be 3 characters or more")
          .max(40, "Must be 40 characters or less")
          .required(title + " Required");
        break;
      case "email":
        valdationSchema = Yup.string()
          .matches(
            /[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/,
            "Invalid Email"
          )
          .required(title + " Required");
        break;
      case "mobile":
        valdationSchema = Yup.string()
          .min(10, "Must be 10 Digits")
          .max(10, "Must be 10 Digits")
          .required(title + " Required");
        break;
      case "pincode":
        valdationSchema = Yup.string()
          .min(6, "Must be 6 Digits")
          .max(6, "Must be 6 Digits")
          .required(title + " Required");
        break;
      case "password":
        valdationSchema = Yup.string()
          .min(5, "Must be 5 characters or more")
          .max(20, "Must be 20 characters or less")
          .matches(/^[a-zA-Z0-9]+$/, "Not a valid password")
          .required("Password Required");
        break;
      case "number":
        valdationSchema = Yup.string().required("Field Required");
        break;
      case "html":
        valdationSchema = Yup.string()
          .min(3, "Must be 3 characters or more")
          .required(title + " Required");
        break;
      case "fulldesc":
        valdationSchema = Yup.string()
          .min(3, "Must be 3 characters or more")
          .required(title + " Required");
        break;
      case "shortdesc":
        valdationSchema = Yup.string()
          .min(3, "Must be 3 characters or more")
          .max(60, "Must be 60 characters or less")
          .required(title + " Required");
        break;
      case "longdesc":
        valdationSchema = Yup.string()
          .min(3, "Must be 3 characters or more")
          .max(100, "Must be 100 characters or less")
          .required(title + " Required");
        break;
      case "file":
        valdationSchema = Yup.string().required(title + " Required");
        break;
      case "url":
        valdationSchema = Yup.string().required(title + " Required");
        break;
      case "date":
        valdationSchema = Yup.string().required(title + " Required");
        break;
      case "time":
        valdationSchema = Yup.string().required(title + " Required");
        break;
      case "datetime":
        valdationSchema = Yup.string().required(title + " Required");
        break;
      case "daterange":
        valdationSchema = Yup.string().required(title + " Required");
        break;
      default:
        valdationSchema = Yup.string().required(title + " Required");
        break;
    }
    return valdationSchema;
  }

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={FormSchema}
      onSubmit={onSubmit}
    >
      {({ errors, values, touched, setValues }) => (
        <Form>
          <div className="row">
            <div className="col-md-12 text-center mb-4">
              <h2>{formSchema["title"]}</h2>
            </div>

            <FieldArray name="tickets">
              {() =>
                formSchema["field"].map((fieldData, i) => {
                  let grid: any;
                  if (fieldData["grid"] && fieldData["grid"] != "") {
                    grid = "col-md-" + 12 / fieldData["grid"] + " mb-4";
                  } else {
                    grid = "col-md-" + 12 / formSchema["grid"] + " mb-4";
                  }
                  return (
                    <RenderField
                      key={i}
                      field={fieldData}
                      grid={grid}
                      index={i}
                    />
                  );
                })
              }
            </FieldArray>
            <button
              className="btn btn-lg btn-gradient-danger btn-block btn-rounded mb-4 mt-5"
              type="submit" 
            >
              Submit
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
}

interface FieldProps {
  field: object;
  grid: string;
  index: number;
}

function RenderField({ field, grid, index }: FieldProps) {
  const id = field["id"] ? field["id"] : field["name"];
  if (field["type"] == "text") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="text"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "email") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="email"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "password") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="password"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "number") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="number"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "textarea") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="text"
          component="textarea"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "html") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="html"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "select") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="text"
          component="select"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        >
          <option value="" label={field["placeholder"]}>
            {field["placeholder"]}
          </option>
          {field["list"].map((optionData, y) => {
            return (
              <option key={y} value={optionData["key"]}>
                {optionData["value"]}
              </option>
            );
          })}
        </Field>
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "radio") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id} className="disblock">
          {field["title"]}
        </label>
        {field["list"].map((optionData, y) => {
          return (
            <>
              <label className="form-check form-check-inline">
                <Field
                  key={y}
                  id={id}
                  type="radio"
                  value={optionData["key"]}
                  tabIndex={index}
                  className="form-check-input"
                  name={field["name"]}
                  required={field["required"]}
                  placeholder={field["placeholder"]}
                />
                <span className="form-check-label">{optionData["value"]} </span>
              </label>
            </>
          );
        })}
      </div>
    );
  } else if (field["type"] == "checkbox") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id} className="disblock">
          {field["title"]}
        </label>
        <label className="form-check form-check-inline">
          <Field
            id={id}
            type="checkbox"
            tabIndex={index}
            className="form-check-input"
            name={field["name"]}
            placeholder={field["placeholder"]}
          />
          <span className="form-check-label">{field["label"]}</span>
        </label>
      </div>
    );
  } else if (field["type"] == "file") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="file"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "url") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="url"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "range") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="range"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "date") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="date"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "time") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="time"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "datetime") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="datetime-local"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  } else if (field["type"] == "daterange") {
    return (
      <div key={index} className={grid}>
        <label htmlFor={id}>{field["title"]}</label>
        <Field
          id={id}
          type="datetime-local"
          tabIndex={index}
          className="form-control"
          name={field["name"]}
          placeholder={field["placeholder"]}
        />
        <p className="formError">
          <ErrorMessage name={field["name"]} />
        </p>
      </div>
    );
  }
}
