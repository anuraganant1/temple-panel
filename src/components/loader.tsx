export default function Loader(){
  return(
    <div className="loadingText">
      <h2>Loading...</h2>
    </div>
  )
}