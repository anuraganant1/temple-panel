import Link from "next/link";
import { useRouter } from "next/router";

function CenterDepartments() {
  const router = useRouter();
  
  const currentRoute = router.asPath.split("/")[2];
  const firstRoute = router.asPath.split("/")[1];
  console.log("currentRoute", currentRoute);
  console.log("firstRoute",  firstRoute);

    const pageMenuList = [
    { id: 1, title: "Menu", route: "menu" },
    { id: 2, title: "Pages List", route: "pagelist" }
  ];
  

  const subscriptionMenuList = [
    { id: 1, title: "Order List", route: "orderlist" },
    { id: 2, title: "Donor List", route: "donorlist" },
    { id: 3, title: "Plan List", route: "planlist" },
    { id: 4, title: "Category Plan List", route: "categoryplanlist" },
    { id: 5, title: "Page List", route: "pagelist" },
    { id: 6, title: "Department List", route: "departmentlist" },
    { id: 7, title: "Temple List", route: "templelist" },
  ];
  return (
    <>
      <nav id="sidebar">
        <ul className="list-unstyled menu-categories" >
          <li className="menu">
            <Link href={"/home"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Dashboard</span>
                </div>
              </a>
            </Link>
          </li>

          <li className="menu" id="accordionPages">
            <a
              href="#pages"
              data-toggle="collapse"
              aria-expanded="false"
              className="dropdown-toggle"
            >
              <div className="">
                <i className="flaticon-left-dot-menu"></i>
                <span>Page</span>
              </div>
              <div>
                <i className="flaticon-right-arrow"></i>
              </div>
            </a>

            {pageMenuList.map((item, index) => {
              return (
                <ul
                  key={index}
                  className={
                    `collapse submenu list-unstyled ` +
                    (firstRoute == "page" ? "show" : "")
                  }
                  id="pages"
                  data-parent="#accordionPages"
                >
                  <li className={currentRoute == item.route ? "active" : ""}>
                    <Link href={`/page/${item.route}`}>
                      <a> {item.title}</a>
                    </Link>
                  </li>
                </ul>
              );
            })}
          </li>

          <li className="menu" id="accordionDepartments">
            <a
              href="#departments"
              data-toggle="collapse"
              aria-expanded="false"
              className="dropdown-toggle"
            >
              <div className="">
                <i className="flaticon-left-dot-menu"></i>
                <span>Subscription</span>
              </div>
              <div>
                <i className="flaticon-right-arrow"></i>
              </div>
            </a>

            {subscriptionMenuList.map((item, index) => {
              return (
                <ul
                  key={index}
                  className={
                    `collapse submenu list-unstyled ` +
                    (firstRoute == "subscription" ? "show" : "")
                  }
                  id="departments"
                  data-parent="#accordionDepartments"
                >
                  <li className={currentRoute == item.route ? "active" : ""}>
                    <Link href={`/subscription/${item.route}`}>
                      <a> {item.title}</a>
                    </Link>
                  </li>
                </ul>
              );
            })}
          </li>

          <li className="menu">
            <Link href={"/events"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Event System</span>
                </div>
              </a>
            </Link>
          </li>
          <li className="menu">
            <Link href={"/events"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Puja Offering</span>
                </div>
              </a>
            </Link>
          </li>
          <li className="menu">
            <Link href={"/home"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Yatra System</span>
                </div>
              </a>
            </Link>
          </li>
          <li className="menu">
            <Link href={"/home"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Online Courses</span>
                </div>
              </a>
            </Link>
          </li>
          <li className="menu">
            <Link href={"/home"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Food Order System</span>
                </div>
              </a>
            </Link>
          </li>
          <li className="menu">
            <Link href={"/home"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Ecommerce System</span>
                </div>
              </a>
            </Link>
          </li>
          <li className="menu">
            <Link href={"/home"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Target Fund System</span>
                </div>
              </a>
            </Link>
          </li>
          <li className="menu">
            <Link href={"/home"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Guest House Booking</span>
                </div>
              </a>
            </Link>
          </li>
          <li className="menu">
            <Link href={"/home"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Occasion System</span>
                </div>
              </a>
            </Link>
          </li>
          <li className="menu">
            <Link href={"/home"}>
              <a
                data-toggle="collapse"
                aria-expanded="true"
                className="dropdown-toggle"
              >
                <div className="">
                  <i className="flaticon-computer-6"></i>
                  <span>Setting</span>
                </div>
              </a>
            </Link>
          </li>
        </ul>
      </nav>
    </>
  );
}

export default CenterDepartments;
