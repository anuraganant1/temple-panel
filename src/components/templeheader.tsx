

function TempleHeader() {

  return (
    <>
        <div className="widget portlet-widget">
          <div className="widget-content widget-content-area">
            <div className="portlet portlet-dark">
              <div className="portlet-title d-flex justify-content-between">
                <div className="p-caption align-self-center">
                  <i className="flaticon-gear mr-1"></i>
                  <span className="caption-subject text-uppercase"> ISKCON Mumbai</span>
                </div>
              </div>
              <div className="portlet-body mb-4">
                <p>International Society for Krishna Consciousness, also popularly known as the Hare Krishna movement is a spiritual society founded by His Divine Grace A.C. Bhaktivedanta Swami Prabhupada in July 1966 in New York.</p>
              </div>
            </div>
          </div>
        </div>
    </>
  )
}

export default TempleHeader