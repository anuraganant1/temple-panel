import { useEffect } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

function MultipleDD(props: any) {
  console.log("props", props);
  let items: any = {};
  let selected: any = {};
  items = props["menuList"];
  selected = props["pageList"];

  useEffect(() => {}, []);

  // a little function to help us with reordering the result
  const reorder = (list, startIndex, endIndex) => {
    const result = list;
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const move = (source, destination, droppableSource, droppableDestination) => {
    console.log(
      "source",
      source,
      destination,
      droppableSource,
      droppableDestination
    );
    const sourceClone = source;
    const destClone = destination;
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
  };

  const grid = 8;

  const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",
    padding: grid * 2,
    margin: `0 ${grid}px ${grid}px 0`,

    // change background colour if dragging
    background: isDragging ? "lightgreen" : "#ededed",

    // styles we need to apply on draggables
    ...draggableStyle,
  });

  const getListStyle = (isDraggingOver) => ({
    background: isDraggingOver ? "white" : "white",
    overflow: "auto",
    padding: grid,
  });

  const state = {
    items: items,
    selected: selected,
  };
  console.log("state", state);

  const id2List = {
    droppable: "items",
    droppable2: "selected",
  };

  const getList = (id) => {
    return state[id2List[id]];
  };

  const onDragEnd = (result) => {
    const { source, destination } = result;
    // dropped outside the list
    if (!destination) {
      return;
    }

    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        getList(source.droppableId),
        source.index,
        destination.index
      );

      if (source.droppableId === "droppable2") {
        selected = items;
      }
    } else {
      const result = move(
        getList(source.droppableId),
        getList(destination.droppableId),
        source,
        destination
      );
      console.log("result", result);
    }
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <div className="row">
        <Droppable droppableId="droppable">
          {(provided, snapshot) => (
            <div
              className="col-xl-8 col-lg-8 col-md-8 col-12 layout-spacing"
              ref={provided.innerRef}
              style={getListStyle(snapshot.isDraggingOver)}
            >
              <h4>Menu List</h4>
              <div className="row ">
                {state.items.map((menu, index) => {
                  return (
                    <>
                      <div key={index} className="col-lg-6 col-md-6 layout-spacing">
                        <div className="widget portlet-widget">
                          <div className="widget-content widget-content-area">
                            <div className="portlet portlet-primary">
                              <div className="portlet-title portlet-primary  d-flex justify-content-between">
                                <div className="caption  align-self-center">
                                  <span className="caption-subject text-uppercase white">
                                    {" "}
                                    {menu.menuName}
                                  </span>
                                </div>
                                {/* <div className="actions  align-self-center">
                                            <a href="javascript:;" className="btn"><i className="flaticon-edit-7"></i> Edit</a>
                                            <a href="javascript:;" className="btn"><i className="flaticon-note-1"></i> Add</a>
                                        </div> */}
                              </div>
                              <div className="portlet-body portlet-common-body">
                                <p className="mb-4">
                                  {menu.pageList.map((item, index) => (
                                    <Draggable
                                      key={index}
                                      draggableId={item.pageId}
                                      index={index}
                                    >
                                      {(provided, snapshot) => (
                                        <div
                                          ref={provided.innerRef}
                                          {...provided.draggableProps}
                                          {...provided.dragHandleProps}
                                          style={getItemStyle(
                                            snapshot.isDragging,
                                            provided.draggableProps.style
                                          )}
                                        >
                                          {item.pageName}
                                        </div>
                                      )}
                                    </Draggable>
                                  ))}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  );
                })}
              </div>
              {provided.placeholder}
            </div>
          )}
        </Droppable>
        <Droppable droppableId="droppable2">
          {(provided, snapshot) => (
            <div
              className="col-xl-4 col-lg-4 col-md-4 col-12 layout-spacing"
              ref={provided.innerRef}
              style={getListStyle(snapshot.isDraggingOver)}
            >
              <h4>Page List</h4>
              {state.selected.map((item, index) => (
                <Draggable
                  key={item.pageId}
                  draggableId={item.pageId}
                  index={index}
                >
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      style={getItemStyle(
                        snapshot.isDragging,
                        provided.draggableProps.style
                      )}
                    >
                      {item.pageName}
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </div>
    </DragDropContext>
  );
}

export default MultipleDD;
