
export const menuSchema = {
  title: "Create Menu",
  url: "",
  method: "post",
  grid: 1,
  field: [
    {
      title: "Menu Name",
      grid: 1,
      name: "menuName",
      value: "",
      type: "text",
      validation: "name",
      hint: "",
      placeholder: "Menu Name",
    },
    {
      title: "Priority",
      name: "priority",
      value: "",
      validation: "number",
      type: "number",
      hint: "",
      placeholder: "Priority",
    },
    {
      title: "Status",
      name: "status",
      type: "radio",
      hint: "",
      value: "1",
      list: [
        { key: "1", value: "Active" },
        { key: "2", value: "Deactive" },
      ],
    },
  ],
};


