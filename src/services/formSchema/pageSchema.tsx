
export const pageSchema = {
  title: "Create Page",
  url: "",
  method: "post",
  grid: 1,
  field: [
    {
      title: "Page Name",
      grid: 1,
      name: "pageName",
      value: "",
      type: "text",
      validation: "name",
      hint: "",
      placeholder: "Page Name",
    },
    {
      title: "Priority",
      name: "priority",
      value: "",
      validation: "number",
      type: "number",
      hint: "",
      placeholder: "Priority",
    },
    {
      title: "Status",
      name: "status",
      type: "radio",
      hint: "",
      value: "1",
      list: [
        { key: "1", value: "Active" },
        { key: "2", value: "Deactive" },
      ],
    },
  ],
};


