import Util from "./util";

const Api = {
  getPath() {
    return Util.getApiPath("DAD_API_URL");
  },

  login(data) {
    // ("data", data)
    const url = this.getPath();
    const parameter = getSerializeParam(data);
    // console.log("parameter", parameter)
    const notifyText = "Loading Content";
    return Util.getRequest(url, parameter, notifyText);
  },
  getSubscribtionList(data) {
    // ("data", data)
    const url = this.getPath();
    const parameter = getSerializeParam(data);
    // console.log("parameter", parameter)
    const notifyText = "Loading Content";
    return Util.getRequest(url, parameter, notifyText);
  },
};

const getSerializeParam = (obj) => {
  const str = Object.keys(obj)
    .map(function (key) {
      return key + "=" + obj[key];
    })
    .join("&");
  const finalstr = "?" + str;
  return finalstr;
};

export default Api;
