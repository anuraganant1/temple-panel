import axios from "axios";
const Util = {
  getHeader() {
    return {
      "content-type": "application/x-www-form-urlencoded",
    };
  },
  getApiPath(apiName: any) {

    const apiUrl = "https://iskconmumbai.com/admin/services/subscriptions.php";
    try {
      return typeof process.env.API === "undefined"
        ? apiUrl
        : process.env.API[apiName];
    } catch (err) {
      console.log("Critical Failure", err.message);
      return apiUrl;
    }
  },
  getRequest(url: any, data: any, notify: any) {
     return new Promise((resolve, reject) => {
      axios({
        method: "get",
        url: url + data,
        headers: this.getHeader(),
      })
        .then(function (response) {
          resolve(response["data"]);
        })
        .catch((e) => {
          console.log("Critical Failure", e.message);
          reject(e.message);
        });
    });
  },
  postRequest(url: any, data: any, notifyText: any) {
    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        url: url + data,
        headers: this.getHeader(),
      })
        .then(function (response) {
          resolve(response["data"]);
        })
        .catch((e) => {
          console.log("Critical Failure", e.message);
          reject(e.message);
        });
    });
  },
};

export default Util;
