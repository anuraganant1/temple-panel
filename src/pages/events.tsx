import Header from "../components/header";
import Footer from "../components/footer";
import TempleHeader from "../components/templeheader";
import Centers from "../components/centers";
import Departments from "../components/centerdepartments";
import ISKForm from "../components/ISKForm";
import { fullSchema } from "../services/formSchema/formschema";

const pulldata = (data) => {
  console.log("pulldata", data); // LOGS DATA FROM CHILD (My name is Dean Winchester... &)
};

export default function PageList() {
  return (
    <>
      <Header />
      <div className="main-container" id="container">
        <div id="content" className="main-content">
          <div className="row layout-spacing">
            <div className="col-lg-12">
              <div className="statbox widget box box-shadow">
                <div className="widget-content widget-content-area">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="row">
                        <div className="col-xl-12 col-md-12 col-sm-12 col-lg-12">
                          <div className="row">
                            <div className="col-xl-12 col-md-12 col-sm-12 col-lg-12">
                              <TempleHeader />
                            </div>
                            <div className="col-x2-1 col-md-12 col-sm-12 col-lg-2">
                              <Centers />
                            </div>
                            <div className="col-xl-2 col-md-12 col-sm-12 col-lg-2">
                              <Departments />
                            </div>
                            <div className="col-xl-8 col-md-12 col-sm-12 col-lg-8">
                              <div className="row margin-bottom-120">
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div className="statbox widget box box-shadow">
                                    <div className="widget-header">
                                      <div className="row">
                                        <div className="col-xl-12 col-md-12 col-sm-12 col-12 layout-spacing">
                                          <div className="statbox widget box box-shadow">
                                            <div className="widget-header border-bottom border-default">
                                              <div className="row">
                                                <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                                                  <h4>Event Management</h4>
                                                </div>
                                              </div>
                                            </div>
                                            <div className="widget-content widget-content-area animated-underline-content">
                                              <ul
                                                className="nav nav-tabs  mb-3"
                                                id="animateLine"
                                                role="tablist"
                                              >
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link active"
                                                    id="animated-underline-home-tab"
                                                    data-toggle="tab"
                                                    href="#animated-underline-home"
                                                    role="tab"
                                                    aria-controls="animated-underline-home"
                                                    aria-selected="true"
                                                  >
                                                    <i className="flaticon-home-fill-1"></i>{" "}
                                                    Home
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link"
                                                    id="animated-underline-profile-tab"
                                                    data-toggle="tab"
                                                    href="#animated-underline-profile"
                                                    role="tab"
                                                    aria-controls="animated-underline-profile"
                                                    aria-selected="false"
                                                  >
                                                    <i className="flaticon-user-7"></i>{" "}
                                                    Profile
                                                  </a>
                                                </li>
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link"
                                                    id="animated-underline-contact-tab"
                                                    data-toggle="tab"
                                                    href="#animated-underline-contact"
                                                    role="tab"
                                                    aria-controls="animated-underline-contact"
                                                    aria-selected="false"
                                                  >
                                                    <i className="flaticon-telephone"></i>{" "}
                                                    Contact
                                                  </a>
                                                </li>
                                              </ul>

                                              <div
                                                className="tab-content"
                                                id="animateLineContent-4"
                                              >
                                                <div
                                                  className="tab-pane fade show active"
                                                  id="animated-underline-home"
                                                  role="tabpanel"
                                                  aria-labelledby="animated-underline-home-tab"
                                                >
                                                  <ISKForm
                                                    formSchema={fullSchema}
                                                    formSchemaValue={{}}
                                                    func={pulldata}
                                                  />
                                                </div>
                                                <div
                                                  className="tab-pane fade"
                                                  id="animated-underline-profile"
                                                  role="tabpanel"
                                                  aria-labelledby="animated-underline-profile-tab"
                                                >
                                                  <div className="media mt-4 mb-3">
                                                    <div className="media-body">
                                                      Cras sit amet nibh libero,
                                                      in gravida nulla. Nulla
                                                      vel metus scelerisque ante
                                                      sollicitudin. Cras purus
                                                      odio, vestibulum in
                                                      vulputate at, tempus
                                                      viverra turpis. Fusce
                                                      condimentum nunc ac nisi
                                                      vulputate fringilla. Donec
                                                      lacinia congue felis in
                                                      faucibus.
                                                    </div>
                                                  </div>
                                                </div>
                                                <div
                                                  className="tab-pane fade"
                                                  id="animated-underline-contact"
                                                  role="tabpanel"
                                                  aria-labelledby="animated-underline-contact-tab"
                                                >
                                                  <p className="dropcap  dc-outline-primary">
                                                    Lorem ipsum dolor sit amet,
                                                    consectetur adipisicing
                                                    elit, sed do eiusmod tempor
                                                    incididunt ut labore et
                                                    dolore magna aliqua. Ut enim
                                                    ad minim veniam, quis
                                                    nostrud exercitation ullamco
                                                    laboris nisi ut aliquip ex
                                                    ea commodo consequat. Duis
                                                    aute irure dolor in
                                                    reprehenderit in voluptate
                                                    velit esse cillum dolore eu
                                                    fugiat nulla pariatur.
                                                    Excepteur sint occaecat
                                                    cupidatat non proident, sunt
                                                    in culpa qui officia
                                                    deserunt mollit anim id est
                                                    laborum.
                                                  </p>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
