import { useRouter } from "next/router";
import { useEffect } from "react";

import Header from "../../components/header";
import Footer from "../../components/footer";
import TempleHeader from "../../components/templeheader";
import Centers from "../../components/centers";
import Departments from "../../components/centerdepartments";

import Dashboard from "../../components/subscription/dashboard";
import TempleList from "../../components/subscription/templelist";
import CategoryPlanList from "../../components/subscription/categoryplanlist";
import PageList from "../../components/subscription/pagelist";
import PlanList from "../../components/subscription/planlist";
import DonorList from "../../components/subscription/donorlist";
import OrderList from "../../components/subscription/orderlist";
import DepartmentList from "../../components/subscription/departmentlist";

function About() {
  const router = useRouter();
  console.log("router", router);
  const { params } = useRouter().query;

  useEffect(() => {}, []);

  return (
    <>
      <Header />
      <div className="main-container" id="container">
        <div id="content" className="main-content">
          <div className="row layout-spacing">
            <div className="col-lg-12">
              <div className="statbox widget box box-shadow">
                <div className="widget-content widget-content-area">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="row">
                        <div className="col-xl-12 col-md-12 col-sm-12 col-lg-12">
                          <div className="row">
                            <div className="col-xl-12 col-md-12 col-sm-12 col-lg-12">
                              <TempleHeader />
                            </div>
                            <div className="col-x2-1 col-md-12 col-sm-12 col-lg-2">
                              <Centers />
                            </div>
                            <div className="col-xl-2 col-md-12 col-sm-12 col-lg-2">
                              <Departments />
                            </div>
                            <div className="col-xl-8 col-md-12 col-sm-12 col-lg-8">
                              {params[0] == "departmentlist" ? (
                                <DepartmentList />
                              ) : params[0] == "orderlist" ? (
                                <OrderList />
                              ) : params[0] == "donorlist" ? (
                                <DonorList />
                              ) : params[0] == "planlist" ? (
                                <PlanList />
                              ) : params[0] == "pagelist" ? (
                                <PageList />
                              ) : params[0] == "templelist" ? (
                                <TempleList />
                              ) : params[0] == "categoryplanlist" ? (
                                <CategoryPlanList />
                              ) : (
                                <Dashboard />
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default About;
