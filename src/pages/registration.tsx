import styles from "../../styles/Login.module.css";
import { useRouter } from "next/router";
import { useRef } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import Link from "next/link";
// import Api from "../services/api";

function Registration(props: any) {
  console.log("loginprops", props);
  const router = useRouter();
  console.log(router);

  const formikRef = useRef();
  const initialValues = {
    fullname: "",
    email: "",
    password: "",
    confirmedPassword: "",
  };
  const FormSchema = Yup.object().shape({
    fullname: Yup.string()
      .min(3, "Must be 3 characters or more")
      .max(40, "Must be 40 characters or less")
      .matches(/^[a-zA-Z_ ]*$/, "Characters Only")
      .required("Full Name Required"),
    email: Yup.string()
      .matches(
        /[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/,
        "Invalid Email"
      )
      .required("Valid Email Required"),
    password: Yup.string()
      .min(5, "Must be 5 characters or more")
      .max(20, "Must be 20 characters or less")
      .matches(/^[a-zA-Z0-9]+$/, "Not a valid password")
      .required("Password Required"),

    confirmedPassword: Yup.string()
      .min(5, "Must be 5 characters or more")
      .max(20, "Must be 20 characters or less")
      .matches(/^[a-zA-Z0-9]+$/, "Not a valid password")
      .required("Password Required")
      .oneOf([Yup.ref("password"), null], "Passwords must match"),
  });

  const handleSubmit = (values, { resetForm }) => {
    console.log("values", values);

    resetForm({ values: "" });
  };

  return (
    <>
      <div className={`${styles.login}`}>
        <Formik
          innerRef={formikRef}
          initialValues={initialValues}
          validationSchema={FormSchema}
          onSubmit={handleSubmit}
        >
          {({ values }) => (
            <Form className={`${styles.formLogin}`}>
              <div className="row">
                <div className="col-md-12 text-center mb-4">
                  <h2>ISKCON Services</h2>
                </div>

                <div className="col-md-12">
                  <label htmlFor="">Full Name</label>
                  <Field
                    type="text"
                    className="form-control"
                    name="fullname"
                    placeholder="Full Name *"
                  />
                  <p className="formError">
                    <ErrorMessage name="fullname" />
                  </p>

                  <label htmlFor="">Email</label>
                  <Field
                    type="email"
                    className="form-control"
                    name="email"
                    placeholder="Email *"
                  />
                  <p className="formError">
                    <ErrorMessage name="email" />
                  </p>

                  <label htmlFor="">Password</label>
                  <Field
                    type="password"
                    className="form-control"
                    name="password"
                    placeholder="Password *"
                  />
                  <p className="formError">
                    <ErrorMessage name="password" />
                  </p>

                  <label htmlFor="">Repeat Password</label>
                  <Field
                    type="password"
                    className="form-control"
                    name="confirmedPassword"
                    placeholder="Repeat Password *"
                  />
                  <p className="formError">
                    <ErrorMessage name="confirmedPassword" />
                  </p>

                  <button
                    className="btn btn-lg btn-gradient-danger btn-block btn-rounded mb-4 mt-5"
                    type="submit"
                  >
                    Register
                  </button>
                  <Link href={"/login"}>
                    <a className="btn btn-lg btn-outline-dark btn-block btn-rounded mb-3">
                      Go Back
                    </a>
                  </Link>
                </div>

                <div className="col-md-12 mb-0 text-center social-icons">
                  <div className="  mb-4 mr-2">&nbsp;</div>
                  <div className="  mb-4 mr-2">&nbsp;</div>
                  <div className="  mb-4 mr-2">&nbsp;</div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </>
  );
}

export default Registration;
