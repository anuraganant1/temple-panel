import styles from "../../styles/Login.module.css";
import { useRouter } from "next/router";
import { useRef } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import Link from "next/link";
// import Api from "../services/api";

function ForgotPassword(props: any) {
  console.log("loginprops", props);
  const router = useRouter();
  console.log(router);

  const formikRef = useRef();
  const initialValues = {
    email: "",
  };
  const FormSchema = Yup.object().shape({
    email: Yup.string()
      .matches(
        /[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/,
        "Invalid Email"
      )
      .required("Valid Email Required"),
  });

  const handleSubmit = (values, { resetForm }) => {
    console.log("values", values);
    
    resetForm({ values: "" });
  };

  return (
    <>
      <div className={`${styles.login}`}>
        <Formik
          innerRef={formikRef}
          initialValues={initialValues}
          validationSchema={FormSchema}
          onSubmit={handleSubmit}
        >
          {({ values }) => (
            <Form className={`${styles.formPassRecovery}`}>
              <div className="row">
                <div className="col-md-12 text-center mb-4">
                  <h2>ISKCON Services</h2>
                </div>

                <div className="col-md-12">
                  <h4>Recover Password</h4>
                  <p>
                    If you forgot your password enter your email and
                    instructions will sent to you!
                  </p>
                </div>

                <div className="col-md-12">
                  <label htmlFor="">Email</label>
                  <Field
                    type="text"
                    className="form-control"
                    name="email"
                    placeholder="Email *"
                  />
                  <p className="formError">
                    <ErrorMessage name="email" />
                  </p>

                  <button
                    className="btn btn-lg btn-gradient-danger btn-block btn-rounded mb-4 mt-5"
                    type="submit"
                  >
                    Reset Password
                  </button>
                  <Link href={"/login"}>
                    <a className="btn btn-lg btn-outline-dark btn-block btn-rounded mb-3">
                      Back
                    </a>
                  </Link>
                </div>

                <div className="col-md-12 mb-0 text-center social-icons">
                  <div className="  mb-4 mr-2">&nbsp;</div>
                  <div className="  mb-4 mr-2">&nbsp;</div>
                  <div className="  mb-4 mr-2">&nbsp;</div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </>
  );
}

export default ForgotPassword;
