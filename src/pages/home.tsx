import Header from "../components/header";
import Footer from "../components/footer";
import TempleHeader from "../components/templeheader";
import Centers from "../components/centers";
import CenterDepartments from "../components/centerdepartments";
import Dashboard from "../components/subscription/dashboard";

import { gql, useQuery } from "@apollo/client";

const USERINFO = gql`
  {
    books {
      name
      id
    }
  }
`;

export default function Home(props: any) {
  const { data, loading, error } = useQuery(USERINFO);

  console.log("DATA", data, loading, error);

  if (data && data["books"]) {
    const alldata = data["books"];
    console.log("alldata", alldata);
  }

  return (
    <>
      <div className="">
        <Header />
        <div className="main-container" id="container">
          <div id="content" className="main-content">
            <div className="row layout-spacing">
              <div className="col-lg-12">
                <div className="statbox widget box box-shadow">
                  <div className="widget-content widget-content-area">
                    <div className="row">
                      <div className="col-lg-12">
                        <div className="row">
                          <div className="col-xl-12 col-md-12 col-sm-12 col-lg-12">
                            <div className="row">
                              <div className="col-xl-12 col-md-12 col-sm-12 col-lg-12">
                                <TempleHeader />
                              </div>
                              <div className="col-x2-1 col-md-12 col-sm-12 col-lg-2">
                                <Centers />
                              </div>
                              <div className="col-xl-2 col-md-12 col-sm-12 col-lg-2">
                                <CenterDepartments />
                              </div>
                              <div className="col-xl-8 col-md-12 col-sm-12 col-lg-8">
                                <Dashboard />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
}

Home.getInitialProps = async (ctx) => {
  // console.log("ctx", ctx);
  return { loginStatus: true };
};
