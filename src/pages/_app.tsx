import type { AppProps } from "next/app";

import Head from "next/head";
import Script from "next/script";
import "../../styles/global.css";

import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
} from "@apollo/client";

const cache = new InMemoryCache();
const link = new HttpLink({
  uri: "https://iskconmumbai.com/admin/graphql/users/graphql.php",
  fetchOptions: {
    mode: "no-cors",
  },
});
console.log("cache", cache);
console.log("link", link);

const client = new ApolloClient({
  cache,
  link,
});

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <ApolloProvider client={client}>
        <Head>
          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no"
          />
          <title>ISKCON Ter Kadamba BACE</title>
          <meta name="description" content="ISKCON Ter Kadamba BACE" />
          <meta property="keywords" content="ISKCON Ter Kadamba BACE" />
          <link rel="icon" type="image/x-icon" href="/img/fevicon.png" />

          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700"
          />

          <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" />
          <link rel="stylesheet" href="/assets/css/plugins.css" />
          <link rel="stylesheet" href="/assets/css/support-chat.css" />
          <link rel="stylesheet" href="/assets/css/design-css/design.css" />
          <link
            rel="stylesheet"
            href="/assets/css/components/portlets/portlet.css"
          />
          <link
            rel="stylesheet"
            href="/plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.css"
          />

          <link rel="stylesheet" href="/plugins/charts/chartist/chartist.css" />
          <link
            rel="stylesheet"
            href="/assets/css/default-dashboard/style.css"
          />
          <link
            href="/assets/css/ui-kit/tabs-accordian/custom-tabs.css"
            rel="stylesheet"
          />
        </Head>

        <div className="default-sidebar">
          <Component {...pageProps} />

          <Script
            src="/assets/js/libs/jquery-3.1.1.min.js"
            strategy="afterInteractive"
          ></Script>
          <Script
            src="/bootstrap/js/popper.min.js"
            strategy="lazyOnload"
          ></Script>
          <Script
            src="/bootstrap/js/bootstrap.min.js"
            strategy="lazyOnload"
          ></Script>
          <Script
            src="/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/plugins/charts/chartist/chartist.js"
            strategy="lazyOnload"
          ></Script>
          <Script
            src="/plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.min.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/plugins/maps/vector/jvector/worldmap_Script/jquery-jvectormap-world-mill-en.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/plugins/calendar/pignose/moment.latest.min.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/plugins/calendar/pignose/pignose.calendar.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/plugins/progressbar/progressbar.min.js"
            strategy="lazyOnload"
          ></Script>
          <Script
            src="/assets/js/default-dashboard/default-custom.js"
            strategy="beforeInteractive"
          ></Script>
        </div>
      </ApolloProvider>
    </>
  );
}

export default MyApp;
