import Login from "./login";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Home from "./home";
import Loader from "../components/loader";

export default function Landing(props: any) {
  const router = useRouter();
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    console.log("props", props, props.loginStatus);
    if (props.loginStatus) {
      console.log("aaaaaaaa");
      setLoaded(true);
    } else {
      router.replace("/login");
      console.log("bbbbbb");
      setLoaded(true);
    }
  });

  if (!loaded) {
    return (
     <Loader/>
    );
  }
  return <>{props.loginStatus ? <Home /> : <Login />}</>;
}

Landing.getInitialProps = async (ctx) => {
  // console.log("ctx", ctx);
  return { loginStatus: false };
};
