import styles from "../../styles/Login.module.css";
import { useRouter } from "next/router";
import Link from "next/link";
import { useRef } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import Api from "../services/api";

function Login(props: any) {
  console.log("loginprops", props);
  const router = useRouter();
  console.log(router);

  const formikRef = useRef();
  const initialValues = {
    email: "",
    password: "",
  };
  const FormSchema = Yup.object().shape({
    email: Yup.string()
      .matches(
        /[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/,
        "Invalid Email"
      )
      .required("Valid Email Required"),
    password: Yup.string()
      .min(5, "Must be 5 characters or more")
      .max(20, "Must be 20 characters or less")
      .matches(/^[a-zA-Z0-9]+$/, "Not a valid password")
      .required("Password Required"),
  });

  const handleSubmit = (values, { resetForm }) => {
    console.log("values", values);
    router.replace("/home");
    Api.login(values);
    resetForm({ values: "" });
  };

  return (
    <>
      <div className={`${styles.login}`}>
        <Formik
          innerRef={formikRef}
          initialValues={initialValues}
          validationSchema={FormSchema}
          onSubmit={handleSubmit}
        >
          {({ values }) => (
            <Form className={`${styles.formLogin}`}>
              <div className="row">
                <div className="col-md-12 text-center mb-4">
                  <h2>ISKCON Services</h2>
                </div>

                <div className="col-md-12">
                  <label htmlFor="">Username</label>
                  <Field
                    type="text"
                    className="form-control"
                    name="email"
                    placeholder="Username *"
                  />
                  <p className="formError">
                    <ErrorMessage name="email" />
                  </p>

                  <label htmlFor="">Password</label>
                  <Field
                    type="password"
                    className="form-control"
                    name="password"
                    placeholder="Password *"
                  />
                  <p className="formError">
                    <ErrorMessage name="password" />
                  </p>

                  <div className="checkbox d-flex justify-content-between mb-4 mt-3">
                    <div className="custom-control custom-checkbox mr-3">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="customCheck1"
                        value="remember-me"
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customCheck1"
                      >
                        Remember
                      </label>
                    </div>
                    <div className={`${styles.forgotPass}`}>
                      <Link href={"/forgotpassword"}>
                        <a className="cursor-pt">Forgot Password?</a>
                      </Link>
                    </div>
                  </div>
                  <button
                    className="btn btn-lg btn-gradient-danger btn-block btn-rounded mb-4 mt-5"
                    type="submit"
                  >
                    Login
                  </button>
                  <Link href={"/registration"}>
                    <a className="btn btn-lg btn-outline-dark btn-block btn-rounded mb-3">
                      Register
                    </a>
                  </Link>
                </div>

                <div className="col-md-12 mb-0 text-center social-icons">
                  <div className="  mb-4 mr-2">&nbsp;</div>
                  <div className="  mb-4 mr-2">&nbsp;</div>
                  <div className="  mb-4 mr-2">&nbsp;</div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </>
  );
}

export default Login;
