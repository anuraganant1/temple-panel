import { useRouter } from "next/router";
import { useEffect } from "react";

import Header from "../../components/header";
import Footer from "../../components/footer";
import TempleHeader from "../../components/templeheader";
import Centers from "../../components/centers";
import Departments from "../../components/centerdepartments";

import Menu from "../../components/page/menu";
import PageList from "../../components/page/pagelist";


function About() {
  const router = useRouter();
  console.log("router", router);
  const { params } = useRouter().query;

  useEffect(() => {}, []);

  return (
    <>
      <Header />
      <div className="main-container" id="container">
        <div id="content" className="main-content">
          <div className="row layout-spacing">
            <div className="col-lg-12">
              <div className="statbox widget box box-shadow">
                <div className="widget-content widget-content-area">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="row">
                        <div className="col-xl-12 col-md-12 col-sm-12 col-lg-12">
                          <div className="row">
                            <div className="col-xl-12 col-md-12 col-sm-12 col-lg-12">
                              <TempleHeader />
                            </div>
                            <div className="col-x2-1 col-md-12 col-sm-12 col-lg-2">
                              <Centers />
                            </div>
                            <div className="col-xl-2 col-md-12 col-sm-12 col-lg-2">
                              <Departments />
                            </div>
                            <div className="col-xl-8 col-md-12 col-sm-12 col-lg-8">
                              {params[0] == "menu" ? (
                                <Menu />
                              ) : <PageList/>
                              }
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default About;
