const { https } = require('firebase-functions');

const { default: next } = require('next');

const isDev = process.env.NODE_ENV !== 'production';
// firebase serve --only functions:iskconBackendServices
const fs = require('fs');
const server = next({
  dev: isDev,
  conf: { distDir: '.next' },

});


const nextjsHandle = server.getRequestHandler();
exports.iskconBackendServices = https.onRequest((req, res) => {
  let domainName = req.headers["x-forwarded-host"] ? '/' + req.headers["x-forwarded-host"] + '.config.js' : '/localhost.config.js';
  domainName = __dirname + domainName;
  console.log("domainName--->", domainName)
  try {
    const fileExists = fs.existsSync(domainName);
    console.log("fileExists", fileExists)
    if (fs.existsSync(domainName)) {
      const configData = require(domainName);
      console.log("configData----------->", configData)
      if (req) {
        req['configData'] = JSON.stringify(configData);
      }
      console.log("file read")
      // return server.prepare().then(() => nextjsHandle(req, res));
    } else {
      console.log("file created")
      fs.writeFile(domainName, 'module.exports={firstName:"James",lastName:"Bond"}', function (err) {

      });
    }
  } catch (err) {
    console.error(err)
  }
  // console.log("nextServerstart:firebasefunctions--->req", req.headers["x-forwarded-host"])
  return server.prepare().then(() => nextjsHandle(req, res));
});
