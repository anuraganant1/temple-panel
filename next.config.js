/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
}

module.exports = nextConfig
module.exports = {
  distDir: '../.next',
}

module.exports = {
  env: {
    API: {
      DAD_API_URL: 'https://iskconmumbai.com/admin/services/subscriptions.php'
    },
  },
};

module.exports = {
  webpack: (config, {
    buildId,
    dev,
    isServer,
    defaultLoaders,
    webpack
  }) => {
    // Note: we provide webpack above so you should not `require` it
    // Perform customizations to webpack config
    config.plugins.push(
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
      }),
    );
    // Important: return the modified config
    return config;
  },
  images: {
    domains: ["iskconmumbai.com", "www.iskconcommunications.org"],
    formats: ['image/avif', 'image/webp']
  }
}